function signature( author, date ) {
    if(date) { print "\n -- " author "  "date "\n\n"}
}

/^\s*$/{ next }
/^##[ ]*\[v?[0-9]+\.[0-9]+\.[0-9]+.*\]/{
    signature(author, date)
    if(match($0, /[0-9]+.[0-9]+.[0-9]+[-0-9a-zA-Z]*/)){
        version = substr($0, RSTART, RLENGTH);
    }
    if(match($0, /[0-9]+-[0-9]+-[0-9]+/)){
        date = substr($0, RSTART, RLENGTH);
        command = "date -R -d " date
        command | getline date
    }
    print package " (" version ") stable; urgency=medium\n";
}
!/^##[ ]*\[v?[0-9]+\.[0-9]+\.[0-9]+.*\]/{
    if(match($0, /^#+[ ]+/)) { $0=substr($0, RLENGTH + 1)}
    if(version) { print "  " $0}
}
END{
    signature(author, date)
}