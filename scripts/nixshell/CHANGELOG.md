# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## Unreleased
Add unreleased changes below. Do not remove this line.

## [v1.3.0] - 2024-12-11
### Added
* Added AWK script to extract changelog section matching specific version.  
  The expected use of the script: 
  `awk -v version="v1.2.3" -f get_changelog_section.awk CHANGELOG.md`.

### Fixed
* Fixed release publishing function (`gitlab.sh`):
  - Fixed bare sh compatibility.
  - Fixed release URL parsing.

[v1.3.0]: https://gitlab.com/CVisionLab/cvl-ci-helpers/compare/gitlab-helpers/shell/v1.2.2...gitlab-helpers/shell/v1.3.0


## [v1.2.2] - 2022-09-19
### Fixed
* Templates now use `CI_API_V4_URL` variable to access API.

[v1.2.2]: https://gitlab.com/CVisionLab/cvl-ci-helpers/compare/gitlab-helpers/shell/v1.2.1...gitlab-helpers/shell/v1.2.2


## [1.2.1] - 2021-04-09
The project is now licensed under the MIT license.

### Added
* Added `telegram.sh` script with a definition of shell function `telegram_notify`.  
  The function can be used to send telegram notifications with custom buttons and supports conditional inclusion of text and buttons.  
  The implementation doesn't use bash-specific syntax and is expected to work in all conforming shells.

[1.2.1]: https://gitlab.com/CVisionLab/cvl-ci-helpers/compare/gitlab-helpers/shell/v1.2.0...gitlab-helpers/shell/v1.2.1


## [1.2.0] - 2021-03-01
### Changed
* Updated awk script for debchangelog to support 'version - date' format (as on keepachangelog.com), possible 'v' in the beginning and suffix in the end of version.

[1.2.0]: https://gitlab.com/CVisionLab/cvl-ci-helpers/compare/gitlab-helpers/shell/v1.1.2...gitlab-helpers/shell/v1.2.0


## [1.1.2] - 2019-12-02
### Fixed
* Fixed `get-jobs` to not fail in case of empty input.
* Fixed "require artifacts" (`-r`) test in `get-artifacts`. Fixed missing 
  variable expansion.

[1.1.2]: https://gitlab.com/CVisionLab/cvl-ci-helpers/compare/gitlab-helpers/shell/v1.1.1...gitlab-helpers/shell/v1.1.2


## [1.1.1] - 2019-11-16
### Fixed
* Fixed compatibility with some shells (busybox, dash, etc.), that don't 
  support dashes in function names (but support them in aliases).

[1.1.1]: https://gitlab.com/CVisionLab/cvl-ci-helpers/compare/gitlab-helpers/shell/v1.1.0...gitlab-helpers/shell/v1.1.1


## [1.1.0] - 2019-10-01
### Added
* Added documentation about required fields in the input JSON.
* Added new helper function to publish release notes. The new function uses 
  new GitLab releases API.

### Fixed
* Replaced calls to `exit` with calls to `return` in usage functions.
* Fixed wrong usage calls (wrong function names).

[1.1.0]: https://gitlab.com/CVisionLab/cvl-ci-helpers/compare/gitlab-helpers/shell/v1.0.1...gitlab-helpers/shell/v1.1.0


## [1.0.1] - 2019-04-26

### Fixed
* Failure in case of empty results (everything filtered out).
* AWK script `debchangelog.awk` now doesn't strip version suffixes (like rc).

[1.0.1]: https://gitlab.com/CVisionLab/cvl-ci-helpers/compare/gitlab-helpers/shell/v1.0.0...gitlab-helpers/shell/v1.0.1


## [1.0.0] - 2018-12-17

The first release of the unix shell Gitlab helpers.
