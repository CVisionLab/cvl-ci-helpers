/^##[ ]+\[?v?([0-9]+\.)+[0-9]+[a-zA-Z.]*\]?[ -]+[0-9]+-[0-9]+-[0-9]+[ ]*$/{
    if(match($0, /v?([0-9]+\.)+[0-9]+[a-zA-Z.]*/)) {
        found_version = substr($0, RSTART, RLENGTH);
        if (found_version == version) {
            started = 1
            was_found = 1
        } else {
            started = 0
        }
    }
}
{ if(started) { print $0} }
END { if (!was_found) { exit 1 } }