<#
.SYNOPSIS
    Get the last successful pipeline for specific git ref, or matching specific git ref.
.DESCRIPTION
    This Cmdlet can be used to retrieve the latest successful pipeline for specific branch or tag.
    Requires valid API token.
.PARAMETER RefMatch
    Regular expression to match ref names against.
    Default is to not filter by ref.
.PARAMETER Project
    Project identifier (number) or project slug (including namespace).
    Default is to use value of CI_PROJECT_ID environment variable.
.PARAMETER PrivateToken
    Private API-token.
    Default is to use value of PRIVATE_CI_TOKEN environment variable.
.PARAMETER Scope
    Scope to search pipeline:
        * tags - Pipelines triggered as a result of tag push events.
        * branches - Pipelines triggered as a result of branch push events.
    The other scopes available in pipelines API make no sense for successful pipelines.
.PARAMETER Ref
    Exact ref name (if known). The RefMatch parameters gives more abilities, but this parameter can be more
    efficient as it is directly supported by GitLab and filtering runs at serer side.
.PARAMETER Limit
    Limit number of returned pipelines
.EXAMPLE
    PS> Get-Pipelines -Ref-Match "^my-library/v[0-9.]+$" -Project xxxxxxx -Private-Token "xxxxxxxxxxxxxx"

    id      : 12345678
    sha     : a123b45678c90defab1234c5de6789fabc0123d4
    ref     : my-library/v0.2.1
    status  : success
    web_url : https://gitlab.com/example/project/pipelines/12345678

#>
function Get-Pipelines {
    [CmdletBinding()]
    param (
        [parameter(Mandatory=$false)]
        [alias("Ref-Match")]
        [string]$RefMatch,

        [parameter(Mandatory=$false)]
        [string]$Project=$env:CI_PROJECT_ID,

        [parameter(Mandatory=$false)]
        [alias("Private-Token", "Token")]
        [string]$PrivateToken=$env:PRIVATE_CI_TOKEN,

        [parameter(Mandatory=$false)]
        [ValidateSet('tags','branches')]
        [string]$Scope,

        [parameter(Mandatory=$false)]
        [string]$Ref,

        [parameter(Mandatory=$false)]
        [int]$Limit
    )

    # build url
    $endpoint = "$env:CI_API_V4_URL/projects/$([uri]::EscapeDataString($Project))/pipelines?status=success&sort=desc&order_by=id"
    if ($Scope) {
        $endpoint += "&scope=$Scope"
    }
    if ($ref) {
        $endpoint += "&ref=$Ref"
    }

    # Set TLS 1.2
    [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

    $pipelines = @()
    # find pipeline
    while($null -ne $endpoint) {
        $endpoint = fixuri (New-Object System.Uri -ArgumentList ($endpoint))
        $req = Invoke-WebRequest $endpoint -Headers @{'PRIVATE-TOKEN'="$PrivateToken"}
        if ($req.StatusCode -ne 200) {
            Write-Error "Failed to call $endpoint"
            Exit 1
        }
        $res = ConvertFrom-Json $req.Content
        $endpoint = getrel($req)
        foreach ($pipeline in $res) {
            if (-not $RefMatch -or $pipeline.ref -match $RefMatch) {
                $pipeline | Add-Member -TypeName "Gitlab-Pipeline" @{Project=$Project}
                $pipelines += $pipeline
                if ($Limit -and $Limit -le $pipelines.Length) {
                    return $pipelines
                }
            }
        }
    }
    return $pipelines
}


<#
.SYNOPSIS
    Download job artifacts archive.
.DESCRIPTION
    Download artifacts archive of the specified job belonging.
    Requires valid API token.
.PARAMETER Job
    Job object to download artifacts from. Job object should be an output of
    Get-Jobs. If used this way gets Project from Job object.
.PARAMETER Project
    Project identifier (number) or project slug (nincluding namespace)
    Default is to use value of CI_PROJECT_ID environment variable.
.PARAMETER PrivateToken
    Private API-token.
    Default is to use value of PRIVATE_CI_TOKEN environment variable.
.PARAMETER Scope
    Scope to search pipeline:
        * tags - Pipelines triggered as a result of tag push events.
        * branches - Pipelines triggered as a result of branch push events.
    The other scopes available in pipelines API make no sense for successful pipelines.
.PARAMETER TargetFile
    Path to the file to save downloaded archive to.
.PARAMETER TargetDirectory
    Path to the directory to save downloaded archive to. Artifacts archive saved
    with the original name obtained from Gitlab if called with job object, or
    as artifacts.zip.
.PARAMETER IfNotExists
    Skip downloading if file already exists and has the same size.
.PARAMETER Required
    Require at least one artifact to be downloaded.
    Fail if no artifacts were received via pipeline.
.PARAMETER SimpleProgress
    Use simple progress (for non native PS shells, like Gitlab).
.EXAMPLE
    PS> Get-Artifacts -Job xxxxxx -Project xxxxxxx -Private-Token "xxxxxxxxxxxxxx" -Target-File artifacts.zip

    Downloading file 'artifacts.zip'
    Downloaded (1928K of 73618K):
    [oooo                                                 ]
.EXAMPLE
    PS> Get-Pipeline -Project xxxxxxx -Private-Token "xxxxxxxxxxxxxx" -Ref "release/my-library" | Get-Jobs -Job-Match "build-mylib-win64" -Artifacts | Get-Artifacts -Target-Directory deps -If-Not-Exists

    Downloading file 'artifacts.zip'
    Downloaded (1928K of 73618K):
    [oooo                                                 ]
#>
function Get-Artifacts {
    [CmdletBinding()]
    param (
        [parameter(Mandatory=$true, ValueFromPipeline=$true, Position=0,
                   ParameterSetName="ObjFile")]
        [parameter(Mandatory=$true, ValueFromPipeline=$true, Position=0,
                   ParameterSetName="ObjDir")]
        [PSTypeName("Gitlab-Job")]$Job,

        [parameter(Mandatory=$true, ValueFromPipeline=$true, Position=0,
                   ParameterSetName="IdFile",
                   ValueFromPipelineByPropertyName=$true)]
        [parameter(Mandatory=$true, ValueFromPipeline=$true, Position=0,
                   ParameterSetName="IdDir",
                   ValueFromPipelineByPropertyName=$true)]
        [int]$Id,

        [parameter(Mandatory=$true, ParameterSetName="ObjFile", Position=1)]
        [parameter(Mandatory=$true, ParameterSetName="IdFile", Position=1)]
        [alias("Target-File")]
        [string]$TargetFile,

        [parameter(Mandatory=$true, ParameterSetName="ObjDir", Position=1)]
        [parameter(Mandatory=$true, ParameterSetName="IdDir", Position=1)]
        [alias("Target-Directory")]
        [string]$TargetDirectory,

        [parameter(Mandatory=$false)]
        [string]$Project=$env:CI_PROJECT_ID,

        [parameter(Mandatory=$false)]
        [alias("Private-Token", "Token")]
        [string]$PrivateToken=$env:PRIVATE_CI_TOKEN,

        [Parameter(Mandatory=$false)]
        [switch]
        [alias("If-Not-Exists")]
        $IfNotExists,

        [Parameter(Mandatory=$false)]
        [switch]
        [alias("Simple-Progress")]
        $SimpleProgress,

        [Parameter(Mandatory=$false)]
        [switch]
        $Required
    )
    BEGIN {
        $artifacts_count = 0
    }
    PROCESS {
        if ($Job) {
            $Id = $Job.Id
            $Project = $Job.Project
        }

        # Set TLS 1.2
        [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

        $artifacts_count += 1

        $uri = fixuri (New-Object "System.Uri" "$env:CI_API_V4_URL/projects/$([uri]::EscapeDataString($Project))/jobs/$Id/artifacts")
        $request = [System.Net.HttpWebRequest]::Create($uri)
        $request.set_Timeout(15000) #15 second timeout
        $request.Headers['PRIVATE-TOKEN'] = $PrivateToken
        $response = $request.GetResponse()

        if (-not $TargetFile) {
            if ($Job) {
                $TargetFile = Join-Path $TargetDirectory $Job.artifacts_file.filename
            } else {
                $TargetFile = Join-Path $TargetDirectory 'artifacts.zip'
            }
        }

        $TargetFile = $ExecutionContext.SessionState.Path.GetUnresolvedProviderPathFromPSPath($TargetFile)

        if ($IfNotExists -and (Test-Path -LiteralPath $TargetFile -PathType leaf) -and ((Get-Item $TargetFile).Length -eq $response.get_ContentLength())) {
            return Get-Item -Path $TargetFile
        }



        $totalLength = [System.Math]::Floor($response.get_ContentLength()/1024)

        Write-Host "Saving artfacts as: $TargetFile (${totalLength}K)"

        $responseStream = $response.GetResponseStream()
        $targetStream = New-Object -TypeName System.IO.FileStream -ArgumentList $TargetFile, Create
        $buffer = new-object byte[] 1MB
        $count = $responseStream.Read($buffer,0,$buffer.length)
        $downloadedBytes = $count
        $reportedMB = 0
        while ($count -gt 0)
        {
            $targetStream.Write($buffer, 0, $count)
            $count = $responseStream.Read($buffer,0,$buffer.length)
            $downloadedBytes = $downloadedBytes + $count
            if ($SimpleProgress) {
                $downloadedMB = [System.Math]::Floor($downloadedBytes / 1024 / 1024)
                if ($downloadedMB -gt $reportedMB) {
                    $reportedMB = $downloadedMB
                    Write-Host "." -NoNewline
                }
            } else {
                Write-Progress -activity "Downloading file '$($TargetFile.split('/') | Select-Object -Last 1)'" -status "Downloaded ($([System.Math]::Floor($downloadedBytes / 1024))K of $($totalLength)K): " -PercentComplete ((([System.Math]::Floor($downloadedBytes / 1024)) / $totalLength) * 100)
            }
        }

        if ($SimpleProgress) {
            Write-Host "OK"
        } else {
            Write-Progress -activity "Finished downloading file '$($TargetFile.split('/') | Select-Object -Last 1)'"
        }

        $targetStream.Flush()
        $targetStream.Close()
        $targetStream.Dispose()
        $responseStream.Dispose()
        return Get-Item -Path $TargetFile
    }
    END {
        if ($artifacts_count -eq 0) {
            Write-Error "No artifacts were downloaded while at least one is required"
            Exit 1
        }
    }
}

<#
.SYNOPSIS
    Get the last successful job with artifacts.
.DESCRIPTION
    Get the latest successful job belonging to the given project that has artifacts, optionally testing
    job name against regexp match.
    Requires valid API token.
.PARAMETER Pipeline
    Pipeline object (output of Get-Pipelines). When used this way gets Project from pipeline.
.PARAMETER Id
    Pipeline Id
.PARAMETER JobMatch
    Regexp to match against job names.
.PARAMETER Project
    Project identifier (number) or project slug (including namespace).
    Default is to use value of CI_PROJECT_ID environment variable.
.PARAMETER PrivateToken
    Private API-token.
    Default is to use value of PRIVATE_CI_TOKEN environment variable.
.PARAMETER Limit
    Limit number of returned jobs
.PARAMETER WithArtifacts
    Consider only jobs with artifacts.
.EXAMPLE
    PS> Get-Jobs -Project xxxxxxx -Private-Token "xxxxxxxxxxxxxxxxxx" -Id xxxxxxxx

    id                  : xxxxxxxxxx
    status              : success
    stage               : test
    name                : mylib-release-tag-win64
    ref                 : my-library/v0.2.1
    tag                 : True
    coverage            :
    created_at          : 10/24/18 8:34:46 AM
    started_at          : 10/24/18 8:35:06 AM
    finished_at         : 10/24/18 8:36:58 AM
    duration            : 111.573536
    user                : @{id=xxxxxx; name=John Doe; username=jd; state=active;
                        avatar_url=https://secure.gravatar.com/avatar/xxxxxx?s=80&d=identicon;
                        web_url=https://gitlab.com/jd; created_at=3/23/17 9:46:05 AM; bio=; location=; public_email=;
                        skype=; linkedin=; twitter=; website_url=http://example.com; organization=}
    commit              : @{id=a123b45678c90defab1234c5de6789fabc0123d4; short_id=a123b456; title=Merge branch 'release/my-library/v0.2' into
                        'master'; created_at=10/24/18 8:33:27 AM; parent_ids=System.Object[]; message=Merge branch 'release/my-library/v0.2' into
                        'master'

                        Prepared v0.2.1 release of my library

                        See merge request example/project!23; author_name=John Doe; author_email=jd@example.com;
                        authored_date=10/24/18 8:33:27 AM; committer_name=John Doe; committer_email=jd@example.com; committed_date=10/24/18
                        8:33:27 AM}
    pipeline            : @{id=xxxxxxxx; sha=a123b45678c90defab1234c5de6789fabc0123d4; ref=my-library/v0.2.1; status=success;
                        web_url=https://gitlab.com/example/project/pipelines/xxxxxxxx}
    web_url             : https://gitlab.com/example/project/-/jobs/xxxxxxxx
    artifacts_file      : @{filename=my-library-v0-2-1-win64.zip; size=75385796}
    artifacts           : {@{file_type=archive; size=75385796; filename=my-library-v0-2-1-win64.zip; file_format=zip}, @{file_type=metadata;
                        size=402; filename=metadata.gz; file_format=gzip}, @{file_type=trace; size=69600; filename=job.log; file_format=}}
    runner              : @{id=xxxxxx; description=win-cmd-x64-vc14-opencv-qt5; ip_address=127.0.0.1; active=True; is_shared=False;
                        name=gitlab-runner; online=True; status=online}
    artifacts_expire_at :
.EXAMPLE
    PS> Get-Pipelines -Ref-Match "^my-library/v[0-9.]+$" -Project xxxxxxx -Private-Token "xxxxxxxxxxxxxx" | Get-Jobs -Private-Token "xxxxxxxxxxxxxxxxxx"

    id                  : xxxxxxxxxx
    status              : success
    stage               : test
    name                : mylib-release-tag-win64
    ref                 : my-library/v0.2.1
    tag                 : True
    coverage            :
    created_at          : 10/24/18 8:34:46 AM
    started_at          : 10/24/18 8:35:06 AM
    finished_at         : 10/24/18 8:36:58 AM
    duration            : 111.573536
    user                : @{id=xxxxxx; name=John Doe; username=jd; state=active;
                        avatar_url=https://secure.gravatar.com/avatar/xxxxxx?s=80&d=identicon;
                        web_url=https://gitlab.com/jd; created_at=3/23/17 9:46:05 AM; bio=; location=; public_email=;
                        skype=; linkedin=; twitter=; website_url=http://example.com; organization=}
    commit              : @{id=a123b45678c90defab1234c5de6789fabc0123d4; short_id=a123b456; title=Merge branch 'release/my-library/v0.2' into
                        'master'; created_at=10/24/18 8:33:27 AM; parent_ids=System.Object[]; message=Merge branch 'release/my-library/v0.2' into
                        'master'

                        Prepared v0.2.1 release of my library

                        See merge request example/project!23; author_name=John Doe; author_email=jd@example.com;
                        authored_date=10/24/18 8:33:27 AM; committer_name=John Doe; committer_email=jd@example.com; committed_date=10/24/18
                        8:33:27 AM}
    pipeline            : @{id=xxxxxxxx; sha=a123b45678c90defab1234c5de6789fabc0123d4; ref=my-library/v0.2.1; status=success;
                        web_url=https://gitlab.com/example/project/pipelines/xxxxxxxx}
    web_url             : https://gitlab.com/example/project/-/jobs/xxxxxxxx
    artifacts_file      : @{filename=my-library-v0-2-1-win64.zip; size=75385796}
    artifacts           : {@{file_type=archive; size=75385796; filename=my-library-v0-2-1-win64.zip; file_format=zip}, @{file_type=metadata;
                        size=402; filename=metadata.gz; file_format=gzip}, @{file_type=trace; size=69600; filename=job.log; file_format=}}
    runner              : @{id=xxxxxx; description=win-cmd-x64-vc14-opencv-qt5; ip_address=127.0.0.1; active=True; is_shared=False;
                        name=gitlab-runner; online=True; status=online}
    artifacts_expire_at :
#>
function Get-Jobs {
    [CmdletBinding()]
    param (
        [parameter(Mandatory=$true, ValueFromPipeline=$true, Position=0,
                   ParameterSetName="InputObj")]
        [PSTypeName("Gitlab-Pipeline")]$Pipeline,

        [parameter(Mandatory=$true, ValueFromPipeline=$true, Position=0,
                   ParameterSetName="InputId",
                   ValueFromPipelineByPropertyName=$true)]
        [int]$Id,

        [parameter(Mandatory=$false)]
        [alias("Job-Match")]
        [string]$JobMatch,

        [parameter(Mandatory=$false)]
        [string]$Project=$env:CI_PROJECT_ID,

        [parameter(Mandatory=$false)]
        [alias("Private-Token", "Token")]
        [string]$PrivateToken=$env:PRIVATE_CI_TOKEN,

        [parameter(Mandatory=$false)]
        [int]$Limit,

        [parameter(Mandatory=$false)]
        [alias("Artifacts")]
        [switch]$WithArtifacts
    )

    PROCESS {
        if ($Pipeline) {
            $Id = $Pipeline.Id
            $Project = $Pipeline.Project
        }

        $endpoint = "$env:CI_API_V4_URL/projects/$([uri]::EscapeDataString($Project))/pipelines/$Id/jobs?scope=success"

        # Set TLS 1.2
        [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

        $jobs = @()
        # find job
        while($null -ne $endpoint) {
            $endpoint = fixuri (New-Object System.Uri -ArgumentList ($endpoint))
            $req = Invoke-WebRequest $endpoint -Headers @{'PRIVATE-TOKEN'="$PrivateToken"}
            if ($req.StatusCode -ne 200) {
                Write-Error "Failed to call $endpoint"
                Exit 1
            }
            $res = ConvertFrom-Json $req.Content
            $endpoint = getrel($req)
            foreach ($job in $res) {
                if ( $WithArtifacts -and -not $job.artifacts_file) {
                    continue
                }
                if (-not $JobMatch -or $job.name -match $JobMatch) {
                    $job | Add-Member -TypeName "Gitlab-Job" @{Project=$Project}
                    $jobs += $job
                    if ($Limit -and $Limit -le $jobs.Length) {
                        return $jobs
                    }
                }
            }
        }
        return $jobs
    }
}


<#
.SYNOPSIS
    Get tags and / or branches of the current or passed commit.
.DESCRIPTION
    Get branches and tags of current commit or commit given by sha, optionally filtering by regexp.
    Requires valid API token.
.PARAMETER RefMatch
    Regexp to match against ref names.
.PARAMETER Project
    Project identifier (number) or project slug (including namespace).
    Default is to use value of CI_PROJECT_ID environment variable.
.PARAMETER PrivateToken
    Private API-token.
    Default is to use value of PRIVATE_CI_TOKEN environment variable.
.PARAMETER Type
    Type of refs to return: tags, branches or all.
.PARAMETER Limit
    Number of refs to return.
    Default is to return single ref.
.PARAMETER CommitSha
    Commit SHA to fetch references.
    Default is to use value of CI_COMMIT_SHA environment variable.
.PARAMETER IncludeSelf
    If current ref name should be left in the returned refs.
    Default is to filter current ref (CI_COMMIT_REF_NAME) from the result.
.EXAMPLE
    PS> Get-Refs -Project xxxxxx -Sha xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -Token "xxxxxxxxxxxxxxxxxxxxxx" -Number 2

    type   name
    ----   ----
    branch master
    tag    my-library/v0.2.1
#>
function Get-Refs {
    [CmdletBinding()]
    param (
        [parameter(Mandatory=$false)]
        [alias("Match", "Search")]
        [string]$RefMatch,

        [parameter(Mandatory=$false)]
        [string]$Project=$env:CI_PROJECT_ID,

        [parameter(Mandatory=$false)]
        [ValidateSet("tag", "branch", "all")]
        [string]$Type="all",

        [parameter(Mandatory=$false)]
        [int]$Limit=1,

        [parameter(Mandatory=$false)]
        [alias("Private-Token", "Token")]
        [string]$PrivateToken=$env:PRIVATE_CI_TOKEN,

        [parameter(Mandatory=$false)]
        [alias("Commit", "Commit-Sha", "Sha")]
        [string]$CommitSha=$env:CI_COMMIT_SHA,

        [parameter(Mandatory=$false)]
        [alias("Include", "Include-Self")]
        [switch]$IncludeSelf
    )

    $endpoint = "$env:CI_API_V4_URL/projects/$([uri]::EscapeDataString($Project))/repository/commits/$CommitSha/refs?type=$Type"
    $endpoint = fixuri (New-Object System.Uri -ArgumentList ($endpoint))

    # Set TLS 1.2
    [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

    $response = Invoke-RestMethod $endpoint -Headers @{'PRIVATE-TOKEN'="$PrivateToken"}

    if (-not $IncludeSelf) {
        $response = $response | Where-Object name -CNE $env:CI_COMMIT_REF_NAME
    }

    if ($RefMatch) {
        $response = $response | Where-Object name -Match $RefMatch
    }
    return $response | Select-Object -First $Limit
}


<#
.SYNOPSIS
    Extract section from file between two lines specified by regular expressions.
.DESCRIPTION
    This Cmdlet can be used to extract section of file with someregular content,
    i.e. Changelog.
    Returns string containing portion of the file starting with the first line
    matched by Start regex and ending with the first line matched by End regex
    or second line matched by Start regex if End is not provided.
.PARAMETER Path
    Path to file.
.PARAMETER Start
    Start line regex.
.PARAMETER End
    End line regex. If not passed uses the seconf match of Start regex as end
    line.
.EXAMPLE
    PS> Get-FileSection CHANGELOG.md
    ## 2018-10-25 v0.0.10

    ### Fixed

    * Fixed some nasty bug.

#>
function Get-FileSection {
    [CmdletBinding()]
    param (
        [parameter(Mandatory=$true, Position=0)]
        [string]$Path,

        [parameter(Mandatory=$false, Position=1)]
        [string]$Start="^##\s+\d{4}-\d{2}-\d{2}\s+\[?v?\d+(\.\d+)*\]?",

        [parameter(Mandatory=$false, Position=2)]
        [string]$End
    )

    $start_match = Select-String $Path -Pattern $Start | Select-Object -First 1

    if (-not $start_match) {
        Write-Error "Start sequence not found"
        Exit 1
    }

    if ($end) {
        $end_match = Select-String $Path -Pattern $End | Select-Object -First 1
    } else {
        $end_match = Select-String $Path -Pattern $Start | Select-Object -First 1 -Skip 1
    }
    $start_line = $start_match.LineNumber
    if (-not $end_match) {
        Write-Host "End sequence not found, defaulting to end of file"
        return Get-Content $Path | Select-Object -Skip ($start_line - 1) | Out-String
    }

    $end_line = $end_match.LineNumber

    return Get-Content $Path | Select-Object -Index (($start_line - 1)..($end_line - 2)) | Out-String
}


<#
.SYNOPSIS
    Add release notes to a tag.
.DESCRIPTION
    Create Gitlab release with provided description.
    This Cmdlet will fail if release already exists.
.PARAMETER Tag
    Existing tag to assign release to.
    Default is to use value of CI_COMMIT_TAG environment variable.
.PARAMETER Project
    Project identifier (number) or project slug (including namespace).
    Default is to use value of CI_PROJECT_ID environment variable.
.PARAMETER PrivateToken
    Private API-token.
    Default is to use value of PRIVATE_CI_TOKEN environment variable.
#>
function Add-ReleaseNotes {
    [CmdletBinding()]
    param (
        [parameter(Mandatory=$true, Position=0)]
        [string]$Description,

        [parameter(Mandatory=$false, Position=1)]
        [string]$Tag=$env:CI_COMMIT_TAG,


        [parameter(Mandatory=$false)]
        [string]$Project=$env:CI_PROJECT_ID,

        [parameter(Mandatory=$false)]
        [alias("Private-Token", "Token")]
        [string]$PrivateToken=$env:PRIVATE_CI_TOKEN
    )
    $params = @{"description"=$Description}
    try {
        $endpoint = "$env:CI_API_V4_URL/projects/$([uri]::EscapeDataString($Project))/repository/tags/$([uri]::EscapeDataString($Tag))/release"
        $endpoint = fixuri (New-Object System.Uri -ArgumentList ($endpoint))

        # Set TLS 1.2
        [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

        Invoke-RestMethod -Method "POST" $endpoint -Headers @{'PRIVATE-TOKEN'="$PrivateToken"} -Body ($params | ConvertTo-Json) -ContentType "application/json"
    } catch {
        Write-Error $_.ErrorDetails
        Exit 1
    }
}


<#
.SYNOPSIS
    Create and publish release (Gitlab 11.7+).
.DESCRIPTION
    Create Gitlab release with the provided description.
    This Cmdlet will fail if release already exists.
.PARAMETER Name
    Release name.
.PARAMETER Description
    Release description markdown.
.PARAMETER Tag
    Existing tag linked to release.
    Default is to use value of CI_COMMIT_TAG environment variable.
.PARAMETER Assets
    Dictionary of release links (assets).
.PARAMETER Project
    Project identifier (number) or project slug (including namespace).
    Default is to use value of CI_PROJECT_ID environment variable.
.PARAMETER PrivateToken
    Private API-token.
    Default is to use value of PRIVATE_CI_TOKEN environment variable.
#>
function New-Release {
    [CmdletBinding()]
    param (
        [parameter(Mandatory=$true, Position=0)]
        [string]$Name,

        [parameter(Mandatory=$true, Position=1)]
        [string]$Description,

        [parameter(Mandatory=$false, Position=2)]
        [alias("Links")]
        [hashtable]$Assets=@{},

        [parameter(Mandatory=$false, Position=3)]
        [string]$Tag=$env:CI_COMMIT_TAG,

        [parameter(Mandatory=$false)]
        [string]$Project=$env:CI_PROJECT_ID,

        [parameter(Mandatory=$false)]
        [alias("Private-Token", "Token")]
        [string]$PrivateToken=$env:PRIVATE_CI_TOKEN
    )
    $assets_links = @()
    foreach ($key in $Assets.keys) {
        $assets_links += @{"name"=$key;"url"=$Assets[$key]}
    }
    $params = @{"description"=$Description; "name"=$Name; "tag_name"=$Tag; "assets"=@{"links"=$assets_links}}
    try {
        $endpoint = "$env:CI_API_V4_URL/projects/$([uri]::EscapeDataString($Project))/releases"
        $endpoint = fixuri (New-Object System.Uri -ArgumentList ($endpoint))

        # Set TLS 1.2
        [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

        Invoke-RestMethod -Method "POST" $endpoint -Headers @{'PRIVATE-TOKEN'="$PrivateToken"} -Body ($params | ConvertTo-Json -depth 10) -ContentType "application/json"
    } catch {
        Write-Error $_.ErrorDetails
        Exit 1
    }
}

# Internal functions

# Stop PS replacing url-encoded characters with their raw alternatives.
function fixuri($uri){
    $UnEscapeDotsAndSlashes = 0x2000000;
    $SimpleUserSyntax = 0x20000;

    $type = $uri.GetType();
    $fieldInfo = $type.GetField("m_Syntax", ([System.Reflection.BindingFlags]::Instance -bor [System.Reflection.BindingFlags]::NonPublic));

    $uriParser = $fieldInfo.GetValue($uri);
    $typeUriParser = $uriParser.GetType().BaseType;
    $fieldInfo = $typeUriParser.GetField("m_Flags", ([System.Reflection.BindingFlags]::Instance -bor [System.Reflection.BindingFlags]::NonPublic -bor [System.Reflection.BindingFlags]::FlattenHierarchy));
    $uriSyntaxFlags = $fieldInfo.GetValue($uriParser);

    $uriSyntaxFlags = $uriSyntaxFlags -band (-bnot $UnEscapeDotsAndSlashes);
    $uriSyntaxFlags = $uriSyntaxFlags -band (-bnot $SimpleUserSyntax);
    $fieldInfo.SetValue($uriParser, $uriSyntaxFlags);
    return $uri
}

# Extract rel link from header of web response
function getrel($response, $type='next') {
    $header_link = $response.Headers.Link
    if (-not $header_link) {
        return $null
    }
    foreach ($link in $header_link.Split(',')) {
        if ($link -match '<([^>]+)>;\s*rel="([^"]+)"') {
            if ($Matches[2] -eq $type) {
                return $Matches[1]
            }
        }
    }
    return $null
}