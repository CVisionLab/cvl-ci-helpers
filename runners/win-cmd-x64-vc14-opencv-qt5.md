# Runner setup

## Environment variables used by the MSVC tools

* `LIB` - Library search paths
* `INCLUDE` - Include search paths
* `PATH` - dynamic libraries (binaries) search paths


## Requirements

1. Download and install Build Tools for Visual Studio.  
   https://www.microsoft.com/en-us/download/details.aspx?id=48159  
   Make sure to select Visual Studio 2015 (v14) build tools.

2. Download and install Qt.  
   https://www.qt.io/download-qt-installer 
   
3. Download and install OpenCV 3.x (https://opencv.org/releases.html)

4. Download and install Cuda 9.0 and patches. The following Cuda packages
   need to be installed:  
   `compiler_9.0`, `command_line_tools_9.0`, `cublas_9.0`, `cublas_dev_9.0`, 
   `cudart_9.0`, `cufft_9.0`, `cufft_dev_9.0`, `curand_9.0`, `curand_dev_9.0`,
   `cusolver_9.0`, `cusolver_dev_9.0`, `cusparse_9.0`, `cusparse_dev_9.0`, 
   `nvgraph_9.0`, `nvgraph_dev_9.0`, `npp_9.0`, `npp_dev_9.0`, `nvrtc_9.0`, 
   `nvrtc_dev_9.0`, `nvml_dev_9.0`

   ```cmd
   <PackageName>.exe -s compiler_9.0 command_line_tools_9.0 cublas_9.0    cublas_dev_9.0 cudart_9.0 cufft_9.0 cufft_dev_9.0 curand_9.0 curand_dev_9.0 cusolver_9.0 cusolver_dev_9.0 cusparse_9.0 cusparse_dev_9.0 nvgraph_9.0 nvgraph_dev_9.0 npp_9.0 npp_dev_9.0 nvrtc_9.0 nvrtc_dev_9.0 nvml_dev_9.0
   ```

   Also install all available patches.

5. Install `git` (choose to make it available in command prompt)
6. Install `clcache` via admin Powershell prompt:
   
   ```powershell
   Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force
   Register-PackageSource -Name nuget.org -Location https://www.nuget.org/api/v2 -ProviderName NuGet
   Install-Package -Name clcache -ProviderName NuGet
   ```



## Runner configuration

1. Add Qt Creator bin folder to the `PATH`: `C:\Qt\Tools\QtCreator\bin` 
   (by default).  
   This makes `jom` accessible from the command line.
2. Add Qt bin path to the `PATH`
3. Add OpenCV to `LIB`, `INCLUDE` and `PATH`
4. Call msvc variable setup script in the `pre_build_script`
5. Export OpenCV version via `OPENCV_VERSION` environment variable to make it 
   available to scrips (libraries include this version in their names).
6. Add `clcache` to `PATH` (depends on version, i.e. `C:\Program Files\PackageManagement\NuGet\Packages\clcache.4.1.0\clcache-4.1.0`).
7. Create cache directory and set `CLCACHE_DIR` environment variable (`C:\gitlab-runner\clcache`)

*Note:* All environment variable setup that requires expansion of existing 
variables should be don in scripting environment (i.e. `pre_build_script`).  
Expansion doesn't work (currently) in corresponding `environment` section.

*Note:* It might be necessary to add `%WindowsSdkDir%bin\x64;%WindowsSdkDir%bin\%WindowsSDKVersion%x64;` to the `PATH`. In some installations compiler can't find `rc.exe`, it solves this issue.

### Example configurations

```toml
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "win-cmd-x64-vc14-opencv3-qt5"
  url = "https://gitlab.com"
  token = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
  executor = "shell"
  pre_build_script = '''
    call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" amd64
    set PATH=C:\Program Files\PackageManagement\NuGet\Packages\clcache.4.1.0\clcache-4.1.0;C:\Qt\Tools\QtCreator\bin;C:\Qt\5.9.6\msvc2015_64\bin;C:\local\opencv\build\x64\vc14\bin;C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v9.0\bin;%WindowsSdkDir%bin\x64;%WindowsSdkDir%bin\%WindowsSDKVersion%x64;%PATH%
    set LIB=C:\local\opencv\build\x64\vc14\lib;C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v9.0\lib\x64;%LIB%
    set INCLUDE=C:\local\opencv\build\include;C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v9.0\include;%INCLUDE%
    set OPENCV_VERSION=343
    set CLCACHE_DIR=C:\gitlab-runner\clcache\vc14\%CI_PROJECT_PATH%
    '''
  # tags = amd64, msvc, vc14, msvc2015, opencv, opencv3, opencv3.4, qt, qt5, qt5.9, win, win64, x64, x86_64, cuda, cuda9, cuda9.0, cmd
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
```

### Runner tags

Assign tags corresponding to installed versions. Assign both specific and 
general tags, for example:

* `qt`, `qt5`, `qt5.8`
* `opencv`, `opencv3`, `opencv3.4`
* `msvc`, `msvc2015`, `vc14`
* `win`, `win64`
* `x64`, `x86_64`, `amd64`
* `cuda`, `cuda9`, `cuda9.0`
* `cmd`
* `clcache`
* `win-cmd-x64-vc14-opencv3-qt5`

*Include runner name as tag to make it possible to select specific runner.*  
*Include executor type (cmd, powershell), because they require different 
scripts.*