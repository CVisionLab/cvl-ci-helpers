"""Setup script."""
from setuptools import setup, find_packages
import sys

needs_pytest = {'pytest', 'test', 'ptr'}.intersection(sys.argv)
pytest_runner = ['pytest-runner'] if needs_pytest else []


setup(
    name='my-package',
    use_scm_version=True,
    setup_requires=['setuptools_scm'] + pytest_runner,
    tests_require=['pytest', 'pytest-timeout'],
    description='My package description',
    author='User Name',
    author_email='email@cvisionlab.com',
    packages=find_packages(exclude=['tests.*']),
    python_requires='>=2.7,!=3.0.*,!=3.1.*,!=3.2.*',
    install_requires=[
        'six'
    ],
    zip_safe=False
)
