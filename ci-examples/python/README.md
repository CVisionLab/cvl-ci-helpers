# Python CI Example

This pipeline uses `cvl-ci-helpers/py3-flake8-tox-coverage` Docker image and
runs the following actions:

- `flake8_errors`
  Checks for severe PEP mistakes. The list of mistakes that are not treated as 
  severe is configured in the CI configuration (ignores the following 
  `flake8` mistakes:  `D2,D4,E309,E402,F401,F403,N8,C9,W`).
- `flake8_warnings`
  Checks for PEP warnings. The list of warnings is configured in the CI 
  configuration (ignores the following `flake8` mistakes:  
  `D200,D202,D203,D209,D400,E309,E402,D413`).
- `py2`
  Runs `tox` with `py2` environment. Actions are configured in the `tox` 
  environment (see below).
- `py3`
  Runs `tox` with `py3` environment. Actions are configured in the `tox` 
  environment (see below).


## Tox environment setup

Tox is configured to run `pytest` with coverage reporting.  
*Package name in the invocation must match real package.*

```ini
[tox]
envlist = py2, py3

[testenv]
deps =
    pytest
    setuptools_scm
    coverage
    pytest-cov
    pytest-env-info
commands = pytest --cov my.package --cov-report=term-missing tests/ {posargs}
```

## CI configuration

CI configuration is in the `python.gitlab-ci.yml` file. File should be renamed
to `.gitlab-ci.yml`.

`python.gitlab-ci.yml`:
```yaml
# Template PEP job.
.py-checks:
  image: "${CI_REGISTRY}/cvisionlab/cvl-ci-helpers/py2-flake8-tox-coverage"
  before_script:
    - python -V  # Print out python version for debugging
  except:
    - tags
  only:
    changes:
      - '**/*.py'

# Check for critical PEP errors.
pep8-errors:
  extends: .py-checks
  script:
  - flake8 --ignore D2,D4,E309,E402,F401,F403,N8,C9,W

# Check for PEP warnings.
pep8-warnings:
  extends: .py-checks
  allow_failure: true
  script:
  - flake8 --ignore D200,D202,D203,D209,D400,E309,E402

# Run Python2 tests
py2-tests:
  extends: .py-checks
  script:
  - tox -e py2
  artifacts:
    reports:
      junit: unittest-*.xml

# Run Python3 tests
py3-tests:
  extends: .py-checks
  script:
  - tox -e py3
  artifacts:
    reports:
      junit: unittest-*.xml
```