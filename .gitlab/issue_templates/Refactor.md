## Problem to solve

(Describe code / architecture issue)

## Further details

(Include use cases, benefits, and/or goals)

## Proposal

## Links / references


<!-- Estimate required time -->
/estimate 1d

/label ~Refactor
