<!-- Use this description template for release MR. -->

## Changes

<!-- Briefly describe features and fixes of the new version -->


### API changes

<!-- 
    List API functions that were added / modified / removed / ... 
    in subsections.
    Explicitly note if API was not changed:
    
**No API changes.**

-->

**API version:** `x.x.x` ([documentation](link-to-api-document)).

#### Added (Changed / Removed / ...)
* API call `something`



### Configuration file changes

<!-- Briefly describe added / modified / removed configuration entries 

**No changes in config files.**

-->


## Version tags

<!-- List tags that should be assigned to the merge commit:

* `product/v0.1.2`

-->


## Author's checklist

- [ ] Source branch is `release/*`.
- [ ] Source branch name contains version information.
- [ ] Target branch is `master` (see `/target_branch` quick action at the end of file).
- [ ] Descriptive not automatically generated MR title.
- [ ] Changes are described in changelog(s).
- [ ] API changes are documented in corresponding documents (if any). 
<!-- 
- [ ] No API changes.
-->
- [ ] Configuration changes have default values (in files *and* in SW) and 
      described in changelog(s).
<!-- 
- [ ] No configuration changes. 
-->
- [ ] Update board label of going-to-be-closed issues to ~Merging.  
      Using quick action (`/board_move ~Merging`) or from the board  
      *The actual close will happen when the changes are merged into master*.


## Tester's checklist

- [ ] Try on several random cases.
- [ ] Check if everything described in changelog(s) works as expected.
- [ ] Run on a testing set of cases and review results. Compare with known 
      good results.
- [ ] Approve MR (only if everything works as expected).


## Reviewer's checklist

- [ ] Changes are described in changelog(s).
- [ ] All discussions are resolved.
- [ ] CI build succeed (if configured).
- [ ] CI build warnings are reviewed.
- [ ] Has correct version in changelogs (unreleased header replaced with 
      date-and-version header).
- [ ] API changes are documented in corresponding documents (if any). 
<!-- 
- [ ] No API changes.
-->
- [ ] Configuration changes have default values (in files *and* in SW) and 
      described in changelog(s).
<!-- 
- [ ] No configuration changes. 
-->
- [ ] MR is approved by a tester.


## Post-merge actions

- [ ] Assign tags to merge commit after the MR merged into master.
- [ ] Merge into develop manually after the MR merged into master.
- [ ] Update labels of the issue(s) that were automatically closed with 
      the merge.  
      Closed issues should not have ~"To Do" ~Doing or ~Merging labels

/label ~Release
/target_branch master

