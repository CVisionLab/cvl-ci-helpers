<!-- Use this description template for bugfix branches. -->

## Bug details

<!-- Briefly describe what this MR is about -->


### API changes

<!-- 
    Normally API must not change in bugfixes. 
    If changes are required:
    - Describe API changes 
    - Describe the reason of changing API in the bugfix. 
    - Add a link to the API document and specify new API version.
 -->
**No API changes.**


### Configuration file changes

<!-- Briefly describe added / modified / removed configuration entries 

**No changes in config files.**

-->


## Related issues

<!-- Mention the issue(s) this MR closes or is related to, also add them to
 the title to automatically change their status, when applicable. -->

Resolves #XXX.


## Target branches

<!-- List branches other then MR target that may benefit from this MR -->
**Only MR target**


## Author's checklist

- [ ] Descriptive not automatically generated MR title.
- [ ] MR title includes issue actions if relevant ("resolves #xxx").
- [ ] Changes are described in changelog(s).
- [ ] Contains isolated set of changes related to the bugfix.
- [ ] Target branch is `develop`, `release/*` or `feature/*` (see `/target_branch` quick action at the
      end of file).
- [ ] No API changes.
<!-- 
- [ ] API changes are documented in corresponding documents (if any). 
-->
- [ ] Configuration changes have default values (in files *and* in SW) and 
      described in changelog(s).
<!-- 
- [ ] No configuration changes. 
-->
- [ ] Update board label of going-to-be-closed issues to ~Merging.  
      Using quick action (`/board_move ~Merging`) or from the board  
      *The actual close will happen when the changes are merged into master*.


## Reviewer's checklist

- [ ] MR title includes issue actions if relevant ("resolves #xxx").
- [ ] Contains isolated set of changes related to the bugfix.
- [ ] Changes are described in changelog(s).
- [ ] All discussions are resolved.
- [ ] CI build succeed (if configured).
- [ ] CI build warnings are reviewed.
- [ ] No API changes.
<!-- 
- [ ] API changes are documented in corresponding documents (if any). 
-->
- [ ] Configuration changes have default values (in files *and* in SW) and 
      described in changelog(s).
<!-- 
- [ ] No configuration changes. 
-->


## Post-merge actions

- [ ] Consider merging manually into other target branches.



/label ~Bug
/target_branch develop

