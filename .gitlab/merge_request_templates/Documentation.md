<!-- Use this description template for doc branches. -->



## Project documentation changes

<!-- Briefly describe what changed in project documentation -->



## Documentation build process changes

<!-- Briefly describe what changed in documentation build process -->
**No changes in build process.**


### API documentation

<!-- 
    If changes are required:
    - Describe API documentation changes
    - Describe the reason of changing API documentation (is API changed?
    was it incoherent with code?).
 -->

**No changes in API documentation.**


### Configuration file documentation

<!-- Briefly describe changes in documentation of configuration file.
-->

**No changes in configuration files documentation.**



## Related issues

<!-- Mention the issue(s) this MR closes or is related to, also add them to
 the title to automatically change their status, when applicable. -->

Resolves #XXX.



## Target branches

<!-- List branches other then MR target that may benefit from this MR -->
**Only MR target**


## Author's checklist

- [ ] Descriptive not automatically generated MR title.
- [ ] MR title includes issue actions if relevant ("resolves #xxx").
- [ ] Contains isolated set of changes related to the project documentation.
- [ ] Source branch is `doc/*`.
- [ ] Target branch is `develop` (see `/target_branch` quick action at the end of file).
- [ ] API documentation is not changed.
<!--
- [ ] All API documentation is consistent:
    * Google Docs document.
    * API itself (source code).
    * In-source documentation.
    * READMEs.
-->
<!--
- [ ] All configuration files documentation is consistent.
    * Google docs.
    * Configuration files itself.
    * READMEs.
-->
- [ ] Configuration files documentation is not changed.



## Reviewer's checklist

- [ ] MR title includes issue actions if relevant ("resolves #xxx").
- [ ] Contains isolated set of changes related to the documentation.
- [ ] Changes are described in changelog(s).
- [ ] All discussions are resolved.
- [ ] CI build succeed (if configured).
- [ ] CI build warnings are reviewed.
<!--
- [ ] All API documentation is consistent:
    * Google Docs document.
    * API itself (source code).
    * In-source documentation.
    * READMEs.
-->
<!--
- [ ] All configuration files documentation is consistent.
    * Google docs.
    * Configuration files itself.
    * READMEs.
-->
- [ ] Configuration files documentation is not changed.



/label ~Doc
/target_branch develop


