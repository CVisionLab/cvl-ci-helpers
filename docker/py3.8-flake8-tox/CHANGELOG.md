# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## Unreleased
Add unreleased changes below. Do not remove this line.


## [1.3.0] - 2023-03-07
### Changed
- Updated packages version of the Alpine-based Python 3.8 Docker image for linting 
  and `tox`-ing python projects.  
  Version number follows version of other images, the set of installed python 
  packages is the same as in similar docker images (in this project):
  + `flake8~=6.0.0`
  + `flake8-docstrings~=1.7.0`
  + `flake8-quotes~=3.3.2`
  + `pep8-naming~=0.13.3`
  + `flake8_tuple~=0.4.1`
  + `flake8-bugbear~=23.2.13`
  + `darglint~=1.8.1`
  + `flake8-comprehensions~=3.10.1`
  + `flake8-gl-codeclimate~=0.2.1`
  + `tox~=4.4.6`

[1.3.0]: https://gitlab.com/CVisionLab/cvl-ci-helpers/compare/docker/py3.8-flake8-tox/1.2.1...docker/py3.8-flake8-tox/1.3.0


## [1.2.1] - 2021-04-09
The project is now licensed under the MIT license.

### Changes
* Docker images are rebuilt to include updated packages.

[1.2.1]: https://gitlab.com/CVisionLab/cvl-ci-helpers/compare/docker/py3.8-flake8-tox/1.2.0...docker/py3.8-flake8-tox/1.2.1


## [1.2.0] - 2021-01-22
### Added
* Added additional `flake8` plugins: `flake8-bugbear`, `darglint`, 
  `flake8-comprehensions`.  
  **New plugins are enabled by default.**
* Added support for codeclimate-comaptible flake8 reports (via 
  `flake8-gl-codeclimate` plugin).
* Docker image now includes `jq`

### Fixed
* Reduced image size by disabling `pip` cache.

[1.2.0]: https://gitlab.com/CVisionLab/cvl-ci-helpers/compare/docker/py3.8-flake8-tox/1.1.0...docker/py3.8-flake8-tox/1.2.0


## [1.1.0] - 2019-11-27
### Added
* Docker image now includes `git`.

[1.1.0]: https://gitlab.com/CVisionLab/cvl-ci-helpers/compare/docker/py3.8-flake8-tox/1.0.0...docker/py3.8-flake8-tox/1.1.0


## 1.0.0 - 2019-11-15
The first release of the Alpine-based Python 3.8 Docker image for linting and 
`tox`-ing python projects.