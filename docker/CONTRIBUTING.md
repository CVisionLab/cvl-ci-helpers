# Docker Images Contribution Guidelines

This repository follows next principles to organize docker images:

- Images should be generic enough to be used in several projects.  
  Each project may have own registry but in many cases we use the same or
  almost the same images so it makes sense to have common images in one 
  repository.  
  At the same time specific images should be in the registry of their 
  repositories.
- Each image should have a separate subdirectory in the `docker`  directory in 
  the repository root.
    + The directory name must use only lowercase letters, numbers and `-`.
    + The directory must contain `Dockerfile`.
    + All files required to build the image should be in the directory (may 
      contain subdirectories).


## Submitting new images

Use Merge Request to submit a new image.

Merge request should include all files required to build an image and also
include changes in the root `README.md` to add proposed image to the list.


## Automatic image building

This repository include CI job to build docker image and to make it available 
in the Registry.

This job is executed for each tag that follows the next scheme:  
`docker/image-name/tag-name`

Here `image-name` must be a name of the corresponding image directory.  
The `tag-name` part is also required and defines tag that should be added to 
the image.

In addition to the specified tag each job also sets `latest` tag.
It means that this setup allows only incremental version assignment.

At the moment Gitlab CI doesn't allow to specify both branch and tag so this
must be manually controlled.

**In particular tags should only be added to master branch.**   
This also enforced with protected tags.