# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## Unreleased
Add unreleased changes below. Do not remove this line.


## 1.3.0 - 2023-03-07
### Added
- An initial release of the Alpine-based Python 3.10 Docker image for linting 
  and `tox`-ing python projects.  
  Version number follows version of other images, the set of installed python 
  packages is the same as in similar docker images (in this project):
  + `flake8~=6.0.0`
  + `flake8-docstrings~=1.7.0`
  + `flake8-quotes~=3.3.2`
  + `pep8-naming~=0.13.3`
  + `flake8_tuple~=0.4.1`
  + `flake8-bugbear~=23.2.13`
  + `darglint~=1.8.1`
  + `flake8-comprehensions~=3.10.1`
  + `flake8-gl-codeclimate~=0.2.1`
  + `tox~=4.4.6`
