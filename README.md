# Continuous Integration Helpers

Repository contains continuous integration docker images and also owns CI 
runners that are to be used in more than one project.

[[_TOC_]]

## Docker images

Gitlab allows to see all images and available tags in the [Registry](https://gitlab.com/CVisionLab/cvl-ci-helpers/container_registry).

See [contribution guidelines](docker/CONTRIBUTING.md) to find out how to add 
new image and how to trigger image build.

### Python images with flake8 and tox

This project includes several python images based on alpine with the same 
set of packages. The images are based on official `python:x.x-alpine` docker 
images and inherit their limitations. In particular, python interpreter there 
is built from sources and python packages that compile binary extension during
installation will fail to install.

The following image paths should be specified to use images in other 
projects:  
`${CI_REGISTRY}/cvisionlab/cvl-ci-helpers/<python-version>-flake8-tox`  
Where `<python-version>` is one of the supported versions (see below).

All images are based on Alpine distribution.

All images include `flake8` (with some plugins) package to make it 
possible to run linting without the need to use tox and also to save some time 
required to install dependencies (in comparison with running from tox).  
In particular the following python packages are pre-installed:

- `flake8`
- `flake8-docstrings`
- `flake8-quotes`
- `pep8-naming`
- `flake8_tuple`
- `flake8-bugbear`  
  _(since 1.2.0)_
- `darglint`  
  _(since 1.2.0)_
- `flake8-comprehensions`  
  _(since 1.2.0)_
- `flake8-gl-codeclimate`  
  _(since 1.2.0)_
- `tox`

In addition to python packages, the images also include these packages:

- `git`  
  *(since versions 1.1.0)*


The following python versions are supported in image names:
1. `py3.5`.
2. `py3.6`.
3. `py3.7`.
4. `py3.8`.
5. `py3.9`.
6. `py3.10`.
6. `py3.11`.
7. `py3` (the same as `py3.10`, as in Ubuntu 22.04).

### Python image with flake8, tox and mypy
This image uses python version available in alpine (currently 3.10). It uses 
development version of python image to build one of `mypy` dependencies to 
keep image size small.

Image path:  
`${CI_REGISTRY}/cvisionlab/cvl-ci-helpers/alpine-py3-flake8-tox-mypy`

Contains the same packages as `*-flake8-tox` images but additionally includes `mypy`.

### Deprecation notice
This repository also includes several now deprecated images. They will be available for use but will not be developed further.

* `${CI_REGISTRY}/cvisionlab/cvl-ci-helpers/py2-flake8-tox-coverage`  
  Deprecated as it was based on a bigger base image. Top-level coverage is normally not required as it is used inside of tox.
* `${CI_REGISTRY}/cvisionlab/cvl-ci-helpers/py3-flake8-tox-coverage`  
  Deprecated as it was based on a bigger base image. Top-level coverage is normally not required as it is used inside of tox.
* `${CI_REGISTRY}/cvisionlab/cvl-ci-helpers/alpine-py3-dev`  
  The image is deprecated in favor of using required alpine-based python
  image with installed build-base as builder image.  
  For example:

  ```docker
  FROM python:3.10-alpine as builder

  RUN apk add --no-cache build-base
  RUN pip3 wheel mypy

  FROM python:3.10-alpine

  COPY --from=builder /*.whl /wheels/
  RUN pip3 install --no-cache --find-links=/wheels wheel /wheels/mypy-*.whl
  ```
  See also [alpine-py3-flake8-tox-mypy/Dockerfile](docker/alpine-py3-flake8-tox-mypy/Dockerfile) for an example of multistage build.
* `${CI_REGISTRY}/cvisionlab/cvl-ci-helpers/py2.7-flake8-tox` and `${CI_REGISTRY}/cvisionlab/cvl-ci-helpers/py2-flake8-tox` (alias)  
  Deprecated as python 2.7 is now deprecated.
* `${CI_REGISTRY}/cvisionlab/cvl-ci-helpers/py3.4-flake8-tox`  
  Deprecated as python 3.4 is outdated and not suported by some packages installed in the images.

### Docker images for building documentation with sphinx
Two images are included: with and without PDF support. PDF support requires LaTeX so the latter image is much bigger. The image with PDF support is based on TinyTeX distribution.
* `sphinx`  
  Python 3 image with `sphinx`, `sphinxcontrib-apidoc` and `sphinx-rtd-theme` pre-installed.
* `sphinx-pdf`  
  Python 3 image with `sphinx`, `sphinxcontrib-apidoc`, `sphinx-rtd-theme`, `TinyTex` and minimal set of the required TeX packages pre-installed.


## Template CI jobs

This repository includes several CI jobs that can be directly used in other
projects via simple include. 

### docker.yml
Corresponds to the simple docker workflow, with only one docker image per 
project.

Contains two auto devops jobs:
1. Automatic job `docker-build-image`.  
   The job builds docker image for commits with tag matching pattern 
   `docker/[0-9a-z.-]*`.
2. Automatic job `docker-assign-latest-tag`.  
   The job assigns `latest` tag to docker image for commits having 
   both `docker/[0-9a-z.-]*` and `docker/latest` tags.

#### How to use
Modify your `.gitlab-ci.yml`:
1. Add `docker` stage to `stages` definition.
2. Include `docker.yml` template (`include` option of CI configuration).
3. Add `docker/Dockerfile` file to your repository with docker image 
   definition.
4. Use tags matching `docker/[0-9a-z.-]*` pattern to build docker images.

##### Sample CI config
```yaml
stages:
  - docker

include:
  - project: 'CVisionLab/cvl-ci-helpers'
    ref: master
    file: '/ci-templates/docker.yml'
```

Additionally, if you need to support `latest` tag for docker images, do the 
following:
1. Copy `scripts/shell/gitlab.sh` from this project to `ci/gitlab.sh` of your 
   project.
2. Define `PRIVATE_CI_TOKEN` environment variable in project settings to 
   contain personal access token with `api` scope (Account Settings ⟶ Access 
   Tokens).  
   Beware that the token impersonates the owning user. You may want to make this variable [protected](https://gitlab.com/help/ci/variables/README#protected-environment-variables) and [masked](https://gitlab.com/help/ci/variables/README#masked-variables).

#### Conventions
1. Project docker registry must be enabled.
2. Project must use tags matching regular expression `/^docker\/[.0-9a-z-]*$/` 
   to build docker images.  
   Sample tags that match this regexp: `docker/1.0.0`, `docker/v1.0.0`, 
   `docker/build-env-v1.0.0`.
3. Docker images are built only for tagged commits. Branches are skipped.
4. Tag `docker/latest` has special meaning and skipped by the 
   `docker-build-image` job.
5. Jobs use stage `docker` so it must be in the stages definition of the 
   project.
6. The `docker-assign-latest-tag` job assumes that commit has both version and
   `docker/latest` tags. This implies fetching tags of the commit with 
   GitLab API. This in turn relies on the following:
   1. GitLab shell helper must exist in the project. By default it tries to 
      find it on the `ci/gitlab.sh` path (can be changed, see below).
   2. GitLab API key is provided via `PRIVATE_CI_TOKEN` environment variable. 
      The variable should be set on CI configuration page in the project 
      settings.
7. The `docker-assign-latest-tag` job can handle situation when both version 
   and `docker/latest` tags are pushed at the same time.  
   This means that at the time `docker-assign-latest-tag` runs, corresponding
   docker image (built by `docker-build-image`) is not ready yet.  
   The job handles this situation by attempting to pull that image multiple 
   times. Until the image successfully pulled or the job timed out.  
   If it is known that docker build job (`docker-build-image`) always takes 
   significant time, it makes sense to setup start delay for the 
   `docker-assign-latest-tag` job. See the next section for details.

#### Configuration
CI includes in GitLab work via deep merging of job definitions. This 
essentially means that configuration of an included job is performed by 
declaring local job with the same name and (re)defining options.

See the next list for the settings that influence jobs defined in this 
template.

* `DOCKER_DIR` environment variable sets path to the directory containing 
  `Dockerfile`. Defaults value is `docker`.
* `DOCKER_RETRY_DELAY` environment variable controls delay between docker pull
  attempts for the `docker-assign-latest-tag` job.
* `GITLAB_HELPER_PATH` environment variable sets path to gitlab shell helper 
  script. Default value is `ci/gitlab.sh`.

Example of overwriting configuration:
```yaml
docker-build-image:
   variables:
      DOCKER_DIR: path/to/dir

docker-assign-latest-tag:
   when: delayed
   start_in: 30m
   variables:
      DOCKER_RETRY_DELAY: 5m
      GITLAB_HELPER_PATH: thirdparties/ci/gitlab.sh
```


### multi-docker.yml
Extended version of the above docker build workflow supporting multiple docker 
images per project.

Contains three auto devops jobs:
1. Automatic job `docker-build-image`.  
   The job builds docker image for commits with tag matching pattern 
   `docker/[0-9a-z-]*/[0-9a-z.-]*`.
2. Automatic job `docker-assign-latest-tag`.  
   The job assigns `latest` tag to docker image for commits having 
   both `docker/[0-9a-z-]*/[0-9a-z.-]*` and `docker/[0-9a-z-]*/latest` tags.
3. Automatic job `docker-tag`.  
   The jobs assigns new image and tag names to the existing docker image.
   Uses special tag syntax: `docker/<dst-img>/<dst-tag>=<src-img>/<src-tag>` 
   to assign `<dst-img>:<dst-tag>` to the existing `<src-img>:<src-tag>` image.

#### How to use
Modify your `.gitlab-ci.yml`:
1. Add `docker` stage to `stages` definition.
2. Include `multi-docker.yml` template (`include` option of CI configuration).
3. Add one or more `docker/<image-name>/Dockerfile` files to your repository
   with docker image definition(s). Where `<image-name>` is the image name 
   part of commit tags.
4. Use tags matching `docker/[0-9a-z-]*/[0-9a-z.-]*` pattern to build docker 
   images.  
   **The second part of the tag corresponds to the directory name.**  
   A commit tagged `docker/build-env/v1.0.0` will spawn build job attempting 
   to build docker image using `docker/build-env/Dockerfile`.
5. Use tags `docker/<dst-img>/<dst-tag>=<src-img>/<src-tag>` to create 
   another name to the existing docker image.

##### Sample CI config
```yaml
stages:
  - docker

include:
  - project: 'CVisionLab/cvl-ci-helpers'
    ref: master
    file: '/ci-templates/multi-docker.yml'
```

Additionally, if you need to support `latest` tag for docker images, do the 
following:
1. Copy `scripts/shell/gitlab.sh` from this project to `ci/gitlab.sh` of your 
   project.
2. Define `PRIVATE_CI_TOKEN` environment variable in project settings to 
   contain personal access token with `api` scope (Account Settings ⟶ Access 
   Tokens).  
   Beware that the token impersonates the owning user. You may want to make this variable [protected](https://gitlab.com/help/ci/variables/README#protected-environment-variables) and [masked](https://gitlab.com/help/ci/variables/README#masked-variables).

#### Conventions
1. Project docker registry must be enabled.
2. Project must use tags matching regular expression 
   `/^docker\/[0-9a-z-]*\/[.0-9a-z-]*$/` to build docker images.  
   Sample tags that match this regexp: `docker/build-env/1.0.0`, 
   `docker/production/v1.0.0`, `docker/build-env/python2.7-v1.0.0`.
3. The first part of the tag becomes image name, the second — image tag.
4. Individual `Dockerfile`-s corresponding to different images are placed in 
   directories with names matching image names inside of top-level `docker` 
   directory (top-level directory can be changed).
5. Docker images are built only for tagged commits. Branches are skipped.
6. Tags `docker/[0-9a-z-]*/latest` have special meaning and skipped by the 
   `docker-build-image` job.
7. Jobs use stage `docker` so it must be in the stages definition of the 
   project.
8. The `docker-assign-latest-tag` job assumes that commit has both version and
   `docker/[0-9a-z-]*/latest` tags with **the same "image name"** part.  
   This implies fetching tags of the commit with 
   GitLab API. This in turn relies on the following:
   1. GitLab shell helper must exist in the project. By default it tries to 
      find it on the `ci/gitlab.sh` path (can be changed, see below).
   2. GitLab API key is provided via `PRIVATE_CI_TOKEN` environment variable. 
      The variable should be set on CI configuration page in the project 
      settings.
9. The `docker-assign-latest-tag` job can handle situation when both version 
   and `docker/[0-9a-z-]*/latest` tags are pushed at the same time.  
   This means that at the time `docker-assign-latest-tag` runs, corresponding
   docker image (built by `docker-build-image`) is not ready yet.  
   The job handles this situation by attempting to pull that image multiple 
   times. Until the image successfully pulled or the job timed out.  
   If it is known that docker build job (`docker-build-image`) always takes 
   significant time, it makes sense to setup start delay for the 
   `docker-assign-latest-tag` job. See the next section for details.
10. The `docker-tag` job uses regular expression with pattern `/^docker\/[.0-9a-z-]*\/[.0-9a-z-]*\=[.0-9a-z-]*\/[.0-9a-z-]*$/` 
    to match and parse tags.
11. The `docker-tag` handles not existing source image and tag the same way as
    the `docker-assign-latest-tag` job. It tries to pull source image until 
    succeeds or timed out.

#### Configuration
CI includes in GitLab work via deep merging of job definitions. This 
essentially means that configuration of an included job is performed by 
declaring local job with the same name and (re)defining options.

See the next list for the settings that influence jobs defined in this 
template.

* `DOCKER_DIR` environment variable sets path to the directory containing
   directories matching image names and containing individual `Dockerfile`-s.
   Defaults value is `docker`.
* `DOCKER_RETRY_DELAY` environment variable controls delay between docker pull
  attempts for the `docker-assign-latest-tag` job.
* `GITLAB_HELPER_PATH` environment variable sets path to gitlab shell helper 
  script. Default value is `ci/gitlab.sh`.

Example of overwriting configuration:
```yaml
docker-build-image:
   variables:
      DOCKER_DIR: path/to/dir

docker-assign-latest-tag:
   when: delayed
   start_in: 30m
   variables:
      DOCKER_RETRY_DELAY: 5m
      GITLAB_HELPER_PATH: thirdparties/ci/gitlab.sh
```


### flake8.yml
Two automatic jobs that run `flake8`. Useful to setup python linting on early 
stages of the project. 

Contains two auto devops jobs:
1. Automatic job `flake8`.  
   The job runs only on changes in `*.py` files if an MR exists, or any push to any branch that doesn't have an open MR. Doesn't run for tagged commits.  
   See also `FLAKE8_RUN` variable description below to alter this behavior. 
   Depends on the existence of `.flake8` file in the project root (job is not 
   executed if file is missing).  
   The file should contain project-specific `flake8` configuration.
2. Automatic job `flake8-warn`.  
   Similar to the above, but allowed to fail (doesn't terminate pipeline on failure).
   Depends on the existence of `.flake8.warn` file in the project root (job is 
   not executed if file is missing).  
   The file should contain project-specific `flake8` configuration of lint 
   issues threated as warnings.

#### How to use
Modify your `.gitlab-ci.yml`:
1. Add `lint` stage to `stages` definition.
2. Include `flake8.yml` template (`include` keyword of CI configuration).
3. Add `.flake8` and optionally `.flake8.warn` files to your repository (see `flake8` manual for configuration options).

##### Sample CI config
```yaml
stages:
  - lint

include:
  - project: 'CVisionLab/cvl-ci-helpers'
    ref: master
    file: '/ci-templates/flake8.yml'
```

#### Conventions
1. `flake8` is executed from python3-based image (can be overwritten).
2. Project must have `.flake8` file in its root directory.
3. Rules specified in `.flake8` are threated as **must**. The pipeline 
   terminated if `flake8` returns error.
4. `flake8` is executed with explicit config file (`.flake8` or 
   or `.flake8.warn` passed via `--append-config` option).
5. Jobs are only executed if the corresponding configuration file exists.
6. Tagged commits are not checked.

#### Configuration
CI includes in GitLab work via deep merging of job definitions. This 
essentially means that configuration of an included job is performed by 
declaring local job with the same name and (re)defining options.

See the next list for the settings that influence jobs defined in this 
template.

* `FLAKE8_CHECK_PATHS` environment variable value is passed to `flake8` 
   command line. Can be used to specify paths that should be checked. 
   Default is to check entire project.
* `FLAKE8_RUN` environment variable.  
  If set alters jobs start conditions.
  Supported values are: `auto` (default), `changes` and `always`.
  - In `auto` mode, jobs run if any `.py` file changes in MR pipelines and always in branch pipelines.
  - In `changes` mode, jobs only run if there are changed `.py` files (this can lead to surprising behavior if using branch pipelines).
  - If set to `always` mode, jobs are executed regardless of changed `.py` files.
  - All modes are configured to perform jobs de-duplication: if both branch and MR pipelines are allowed by the project configuration, branch pipeline is skipped if MR exists.

Example of configuration overwriting:
```yaml
flake8:
   variables:
      FLAKE8_RUN: always
      FLAKE8_CHECK_PATHS: research
```

### mypy.yml
An automatic job that runs `mypy`. Useful to setup python type checking on early stages of the project. 

Contains an auto devops job:
1. Automatic job `mypy`.  
   The job runs only on changes in `*.py` files and skips tagged commits. 
   Depends on the existence of `.mypy` file in the project root (job is not 
   executed if file is missing).  
   The file should contain project-specific `mypy` configuration.

#### How to use
Modify your `.gitlab-ci.yml`:
1. Add `lint` stage to `stages` definition.
2. Include `mypy.yml` template (`include` option of CI configuration).
3. Add `.mypy` file to your repository with `mypy` configuration.

##### Sample CI config

```yaml
stages:
  - lint

include:
  - project: 'CVisionLab/cvl-ci-helpers'
    ref: master
    file: '/ci-templates/mypy.yml'
```

#### Conventions
1. `mypy` is executed from python3-based image (can be overwritten).
2. Project must have `.mypy` file in its root directory.
3. Rules specified in `.mypy` are threated as **must**. The pipeline in 
   terminated if `flake8` returns error (can be overwritten).
4. `mypy` is executed with explicit config file: `.mypy`.
5. Job is only executed if corresponding configuration file exist.
6. Tagged commits are not checked.

#### Configuration
CI includes in GitLab work via deep merging of job definitions. This 
essentially means that configuration of an included job is performed by 
declaring local job with the same name and (re)defining options.

See the next list for the settings that influence jobs defined in this 
template.

* `MYPY_CHECK_PATHS` environment variable value is passed to `mypy` 
   command line. Can be used to specify paths that should be checked. 
   Default is to check entire project.

Example of overwriting configuration:

```yaml
mypy:
   variables:
      MYPY_CHECK_PATHS: research
   allow_failure: true
```

### telegram.yml
This template enables automatic Telegram notifications for all pipelines.

Contains two auto dev-ops jobs:
1. Automatic job `telegram-notify-success`.  
   The job invoked at the very last stage `.post` if the previous job succeeds.
2. Automatic job `telegram-notify-failure`.  
   The job invoked at the very last stage `.post` if the previous job fails.

#### How to use
Modify your `.gitlab-ci.yml`:
1. Include `telegram.yml` template (`include` option of CI configuration).
2. Add `TELEGRAM_CHAT_ID` variable in project settings and set it to the id of the Telegram chat that should receive notifications.
3. Define `TELEGRAM_BOT_TOKEN` variable with the token of your bot.  
   **External projects only. CVisionLab projects are already configured to use the right bot.**

##### Sample CI config
```yaml
include:
  - project: 'CVisionLab/cvl-ci-helpers'
    ref: master
    file: '/ci-templates/telegram.yml'
```

#### Conventions
1. Environment variable `TELEGRAM_CHAT_ID` must be defined and must contain ID of the Telegram chat that should receive notifications.  
   *Use command `curl https://api.telegram.org/<bot-token>/getUpdates` just after adding the bot to the chat or group to find chat/group ID.* 
2. Telegram notifications are sent on behalf of the [CVL GitLab Bot](https://t.me/cvl_gitlab_bot).  
   **Internal to CVisionLab**
3. If the commit message contains any of `[mute]`, `[no_bell]`, `[silent]`, `:mute:`, `:no_bell:` or `:silent:`, the notification job is not triggered.

#### Configuration
CI includes in GitLab work via deep merging of job definitions. This 
essentially means that configuration of an included job is performed by 
declaring local job with the same name and (re)defining options.

See the next list for the settings that influence jobs defined in this 
template.

* `TELEGRAM_SEND_TIMEOUT` environment variable value is passed to `curl` and defines send timeout when interacting with Telegram API.  
  Default value is `10`.
* `TELEGRAM_CHAT_ID` environment variable containing destination chat / group ID.  
  Normally should be set in project settings.
* `TELEGRAM_BOT_TOKEN` environment variable can be used to set Telegram bot token (and bot) used to send messages.
* `TELEGRAM_BUTTONS_CONFIG` variable defines visible buttons, their order and 
  rows.  
  Default is `Pipeline,Report,Commit,MR;Deployment`.  
  Commas define buttons order within a single row. Semicolons define button
  rows.
* `TELEGRAM_DRY_RUN` variable controls if telegram notifications are actually sent.  
  Non-empty values result in no messages delivered but instead only printed to the local console.
* `TELEGRAM_EXTRA_BUTTONS_LAYOUT` variable defines layout of the extra buttons.  
  Supported values are `rows` (each extra button on a separate row), `cols` (all extra buttons on a single row) or `none` (extra buttons ignored).
* `TELEGRAM_BUTTONS_BUTTON_*` variables defines additional buttons.  
  _`*` symbol above is a wildcard symbol._  
  Each variable defines single "extra" button to add to the message. The value of the variable should conform to the following format: `button-name=button-url`.

##### Deprecated Environment Variables
* Variable `TELEGRAM_BUTTONS_EXTRA` is deprecated and should not be used in new code.

Example of overwriting configuration:
```yaml
.telegram:
   variables:
      TELEGRAM_BOT_TOKEN: <project-specific-bot-token>
```

#### Deployments (GitLab environments)
Telegram jobs can display information about environments but this requires additional configuration.

GitLab provides a set of variables related to deployments (environments) but these variables are only available to the jobs performing deployments.

To make these variables available to Telegram jobs the following additional steps must be performed:
1. Save environment variables to a file:
   
   ```bash
   echo "CI_ENVIRONMENT_NAME=$CI_ENVIRONMENT_NAME" >> .deployment.env
   echo "CI_ENVIRONMENT_URL=$CI_ENVIRONMENT_URL" >> .deployment.env
   ```
   The name of the file doesn't matter, but it should be the same as in the 
   following section.
2. Declare generated file as a `dotenv` artifact: 
   
   ```yaml
   artifacts:
     reports:
       dotenv: .deployment.env
   ```
3. Make Telegram jobs depend on your deployment jobs to pass (dotenv) 
   artifacts:
   
   ```yaml
   .telegram:
     dependencies:
       - deploy-myapp-prod
       - deploy-myapp-staging
       - deploy-myapp-dev
   ```

### package_registry.yml
Contains `.build-python-package` job template that can be used to build and publish binary python packages
to GitLab Python Package Registry.

The job uses `bdist_wheel` setuptools extension from the `wheel` package to build binary package 
and `twine` package to publish built package to the registry.

#### How to use
Modify your `.gitlab-ci.yml`:
1. Include `package_registry.yml` template (`include` option of CI configuration).
2. Create job based on `.build-python-package` template (*See the following section with example*)
3. Set `PACKAGE_DIRECTORY` variable in job settings.


##### Sample CI config
```yaml
include:
  - project: 'CVisionLab/cvl-ci-helpers'
    ref: master
    file: '/ci-templates/package_registry.yml'

stages:
  - package

foo-server-pkg:
  extends: .build-python-package
  except:
    refs:
      - schedules
  only:
    changes:
      - production/foo_server/**/*
      - .gitlab-ci.yml
  needs: []
  variables:
      PACKAGE_DIRECTORY: "production/foo_server"
```

#### Conventions
1. `.build-python-package` is just a template (Gitlab doesn't run templates). You need to create a job based on this template.
2. Job based on `.build-python-package` is executed from python:3.8-slim-buster image (can be overwritten, only python 3.x).
3. Environment variable `PACKAGE_DIRECTORY` must be defined and must contain path to directory with setup.py file.
4. Jobs based on `.build-python-package` execute `setup.py` explicitly (e.g. `python3 setup.py bdist_wheel`).

#### Configuration
The following environment variables defined in the inherited jobs influence execution:
* `PACKAGE_DIRECTORY` environment variable must be specified and must point to a directory containing `setup.py file`.


### calc-issue-time.yml

Contains `report-issue-time` job. The job generates four artifacts:
- **ci/issues_as_rows.csv** (information for each issue and for each developer)
- **ci/issues_as_columns.csv** (same, but issues saved as columns)
- **ci/stats.csv** (contains total estimated and spent times by participants)
- **ci/mergerequests.csv** (like issues_as_columns, but for merge requests)

#### How to use

1. Include `calc-issue-time.yml` template (`include` option of CI configuration).
2. Add stage `report`.
3. Set environment variable `NUMBER_OF_LAST_DAYS` in CI with value which equal
to number of days (including today) for adding to report.
4. Set environment variable `GITLAB_TOKEN` in CI with your token
for GitLab API access (for reading repository).
5. Set schedule with variable `SCHEDULE_TYPE` which contains string "time_report".

#### Sample CI config

```yaml
include:
  - project: 'CVisionLab/cvl-ci-helpers'
    ref: master
    file: '/ci-templates/calc-issue-time.yml'

stages:
  - report
```

#### Conventions

The environment variable `GITLAB_TOKEN` must contain a token for GitLab API
access with `read_api` scope. That token must be created by one of the project maintainers in his/her GitLab Profile.

If `NUMBER_OF_LAST_DAYS` is not defined, report will be generated for all project time.


### release-notes.yml
This template enables automatic publishing of release notes and release artifacts.

Contains one auto dev-ops job `publish-release` defined at `publish` stage.

#### How to use
Modify your `.gitlab-ci.yml`:
1. Add stage `publish` (or make sure it exists).
2. Include `release-notes.yml` template (`include` option of CI configuration).
3. Put public artifacts to `release-artifacts` directory in the project root.

##### Sample CI config
```yaml
stages:
  - publish

include:
  - project: 'CVisionLab/cvl-ci-helpers'
    ref: master
    file: '/ci-templates/release-notes.yml'
```

Complete example with a sample job pretending to build a public artifact:
```yaml
stages:
  - build
  - publish

include:
  - project: 'CVisionLab/cvl-ci-helpers'
    ref: master
    file: '/ci-templates/release-notes.yml'

build-artifact:
  stage: build
  script:
    - mkdir release-artifacts
    - touch release-artifacts/dummy-artifact.deb
    - mkdir release-artifacts/python
    - touch release-artifacts/python/dummy-package.whl
  artifacts:
    paths:
      - release-artifacts
```

The pipeline defined above will publish release artifacts named `dummy-artifact.deb` and `python/dummy-package.whl` everytime a new git tag will be created. The release notes text will be extracted from the `CHANGELOG.md` file (there must be a version corresponding to the tag for the job to succeed).


#### Conventions
1. The job runs for any tag (can be changed by overriding `rules:` section).
2. The job extracts release notes from the `CHANGELOG.md` file. Keep-a-changelog format of the changelog is assumed.
3. The job extracts release notes that have the same version as in the current commit tag (`$CI_COMMIT_TAG`).
4. Release with empty release notes considered invalid (the job will fail).
5. The job is ready to be used in multi product projects but additional setup is required (a correct path to the `CHANGELOG.md` for each project must be provided). Single product projects should work out of the box.
6. If the current tag include `/` symbol, then:
   - Everything after the last `/` is considered a version (must match changelog's version).
   - All `-`, `_` and `/` symbols in the tag name are replaced with spaces. 
   - The first symbols of the resulting string is capitalized. 
   - The result is used as release name.

#### Configuration
CI includes in GitLab work via deep merging of job definitions. This 
essentially means that configuration of an included job is performed by 
declaring local job with the same name and (re)defining options.

See the next list for the settings that influence jobs defined in this 
template.

* `CHANGELOG` environment variable defined path to the `CHANGELOG.md` file.  
  Default value is `CHANGELOG.md` (in the project root).
* `RELEASE_ARTIFACTS_DIR` variable defines path to the directory with release artifacts.  
  The directory may include subdirectories. All files inside will be published as release artifacts of `package` type. Files names in published artifacts are shown relative to the `RELEASE_ARTIFACTS_DIR` path.  
  Default value is `release-artifacts`.
* `RELEASE_TITLE` variable can be defined to override built-in title composition.  
  A convenient way to define `RELEASE_TITLE` variable is to `export` it in the `before_script` section. This job doesn't use `before_script` so it is safe to override it.

Example of overwriting configuration:
```yaml
publish-release:
  variables:
    CHANGELOG: products/server/CHANGELOG.md
  rules:
    - if: '$CI_COMMIT_TAG =~ /server\/^v\d+\.\d+\.\d+/'
```

With the above configuration, the job will search for release notes in the `products/server/CHANGELOG.md` file and will only fire for tags matching the regular expression.

### flakeheaven.yml
Defines automatic CI job to lint python (also markdown and rst) code using [flakeheaven](https://flakeheaven.readthedocs.io/index.html).

In comparison with `flake8`, `flakeheaven` allows fine-grained control over enabled rules and explicit notion of enabled plugins. Additionally, it uses cache to speedup execution. This is especially useful with slow plugins like `darglint`.

Another feature of `flakeheaven` is that it uses standard `pyproject.toml` file for its configuration.

The `flakeheaven` application is essentially a wrapper around `flake8` so it supports all plugins supported by `flake8`.

The template contains one auto devops job `flakeheaven`.  
The job runs only on changes in `*.py` files if an MR exists, or any push to any branch that doesn't have an open MR. Doesn't run for tagged commits.  
See also `FLAKEHEAVEN_RUN` variable description below to alter this behavior.
   
#### How to use
Modify your `.gitlab-ci.yml`:
1. Add `lint` stage to `stages` definition.
2. Include `flakeheaven.yml` template (`include` keyword of CI configuration).
3. _[optional]_ Add `pyproject.toml` file with `[tool.flakeheaven*]` sections to configure the linter. See [official documentation](https://flakeheaven.readthedocs.io/config.html) for the details.

##### Sample CI config
```yaml
stages:
  - lint

include:
  - project: 'CVisionLab/cvl-ci-helpers'
    ref: master # Use tag name to pin specific version
    file: '/ci-templates/flakeheaven.yml'
```

#### Conventions
1. `flakeheaven` is executed from python3-based image (can be overwritten).
2. If the project doesn't have `pyproject.toml` file in its root directory, a default configuration file defined in the [flakeheaven.default.toml](ci-templates/flakeheaven.default.toml) file is used.
3. If `.flakeheaven.baseline` file exists in the project root directory, the file is used as the [baseline file](https://flakeheaven.readthedocs.io/commands/baseline.html) (using `--baseline` option). The name of the file is configurable.
4. Tagged commits are not checked.
5. Linting results are both exposed to the job output and as code quality artifacts in GitLab format.
6. Linting results are cached and expire in 7 days.  
   The 7 days expiration time is defined by `FLAKEHEAVEN_CACHE_TIMEOUT` variable.
7. The linting is executed at the project root by default.
8. The job display `flakeheaven` configuration and checks that all enabled plugins are installed.  
   The displayed configuration includes all installed and enabled plugins and their rules.  
   The job will fail if any of the configured plugins that is not installed.
9. The auto devops job `flakeheaven` is inherited from the template job `.flakeheaven` to allow referencing parts of the job when overwriting its configuration.

#### Configuration
CI includes in GitLab work via deep merging of job definitions. This 
essentially means that configuration of an included job is performed by 
declaring local job with the same name and (re)defining options.

See the next list for the settings that influence jobs defined in this 
template.

* `FLAKEHEAVEN_PROJECT_DIR` if defined, makes job to change directory to the specified directory before executing `flakeheaven`.  
  By default, `flakeheaven` looks for the `pyproject.toml` in the current directory (then in the parent directory, then in the parent of parent and so on). This allows to have different configurations for different subprojects within the repository.
* `FLAKEHEAVEN_BASELINE_FILE` variable defines name of baseline file.  
  The job checks existence of the file and only adds corresponding command line argument if the file exists.
* `FLAKEHEAVEN_DISPLAY_FORMAT` variable influences linter output in CI job.  
  Default value is `grouped`. See [flakeheaven documentation](https://flakeheaven.readthedocs.io/formatters.html) for the list of allowed formatters.
* `FLAKEHEAVEN_RUN` environment variable.  
  If set alters jobs start conditions.
  Supported values are: `auto` (default), `changes` and `always`.
  - In `auto` mode, jobs run if any `.py` file changes in MR pipelines and always in branch pipelines.
  - In `changes` mode, jobs only run if there are changed `.py` files (this can lead to surprising behavior if using branch pipelines).
  - If set to `always` mode, jobs are executed regardless of changed `.py` files.
  - All modes are configured to perform jobs de-duplication: if both branch and MR pipelines are allowed by the project configuration, branch pipeline is skipped if MR exists.
* `FLAKEHEAVEN_FALLBACK_SETTINGS` variable defines URL of base configuration used if the project doesn't include own `pyproject.toml`.
* `FLAKEHEAVEN_CACHE_TIMEOUT` variables defines cache timeout.  
  Default value is `604800` seconds (7 days).

Example of configuration overwriting:
```yaml
flakeheaven:
   variables:
      FLAKEHEAVEN_RUN: always
      FLAKEHEAVEN_PROJECT_DIR: research
```

Example of installing additional `flake8` plugins:
```yaml
flakeheaven:
   before_script:
     - pip install flake8-use-fstring flake8-length
     - !reference [.flakeheaven, before_script]
```

### ruff.yml
Defines automatic CI job to lint python code using extremely fast Python linter [ruff](https://docs.astral.sh/ruff/).

- ⚡️ 10-100x faster than existing linters
- 🐍 Installable via `pip`
- 🛠️ `pyproject.toml` support
- 🤝 Python 3.11 compatibility
- 📦 Built-in caching, to avoid re-analyzing unchanged files
- 📏 Over 700 built-in rules
- ⚖️ Near-parity with the built-in Flake8 rule set
- 🔌 Native re-implementations of dozens of Flake8 plugins, like flake8-bugbear
- 🌎 Monorepo-friendly, with hierarchical and cascading configuration

The template contains one auto devops job `ruff`.  
The job runs only on changes in `*.py` files if an MR exists, or any push to any branch that doesn't have an open MR. Doesn't run for tagged commits.  
See also `RUFF_RUN` variable description below to alter this behavior.
   
#### How to use
Modify your `.gitlab-ci.yml`:
1. Add `lint` stage to `stages` definition.
2. Include `ruff.yml` template (`include` keyword of CI configuration).
3. Add `pyproject.toml` file with `[tool.ruff]` sections to configure the linter. See [official documentation](https://docs.astral.sh/ruff/configuration/) for the details.

##### Sample CI config
```yaml
stages:
  - lint

include:
  - project: 'CVisionLab/cvl-ci-helpers'
    ref: master # Use tag name to pin specific version
    file: '/ci-templates/ruff.yml'
```

#### Conventions
1. `ruff` is executed from the image maintained by GitLab.
2. Tagged commits are not checked.
3. Linting results are both exposed to the job output and as code quality artifacts in GitLab format.
4. The linting is executed at the project root by default.
9. The auto devops job `ruff` is inherited from the template job `.ruff` to allow referencing parts of the job when overwriting its configuration.

#### Configuration
CI includes in GitLab work via deep merging of job definitions. This 
essentially means that configuration of an included job is performed by 
declaring local job with the same name and (re)defining options.

See the next list for the settings that influence jobs defined in this 
template.

* `RUFF_PROJECT_DIR` if defined passed as command line argument to `ruff`.  
  By default, `ruff` looks for the `pyproject.toml` in the passed directory (then in the parent directory, then in the parent of parent and so on). This allows to have different configurations for different subprojects within the repository.
* `RUFF_RUN` environment variable.  
  If set alters jobs start conditions.
  Supported values are: `auto` (default), `changes` and `always`.
  - In `auto` mode, jobs run if any `.py` file changes in MR pipelines and always in branch pipelines.
  - In `changes` mode, jobs only run if there are changed `.py` files (this can lead to surprising behavior if using branch pipelines).
  - If set to `always` mode, jobs are executed regardless of changed `.py` files.
  - All modes are configured to perform jobs de-duplication: if both branch and MR pipelines are allowed by the project configuration, branch pipeline is skipped if MR exists.
* `RUFF_DOCKER_VERSION` defines version of the docker image to use.  
  Default value is `0.25.0`. See https://hub.docker.com/r/pipelinecomponents/ruff/tags for the list of versions. Version of the image doesn't match version of `ruff`.
* `RUFF_NO_DEFAULT_JOB` disables default job `ruff`.  
  Useful in repositories with multiple independent subprojects. Allows to have multiple jobs with more meaningful names.

Example of configuration overwriting:
```yaml
ruff:
   variables:
      RUFF_RUN: always
      RUFF_PROJECT_DIR: research
```

Example of disabling default job and declaring two jobs for subprojects:
```yaml
variables:
   RUFF_NO_DEFAULT_JOB: 1

ruff-project-1:
   extends: .ruff
   variables:
      RUFF_PROJECT_DIR: project-1/

ruff-project-2:
   extends: .ruff
   variables:
      RUFF_PROJECT_DIR: project-2/
```

### chat-review.yml
Defines automatic CI job to review code using ChatGPT based tool ["Chat Review"](https://github.com/ikoofe/chat-review).
It automatically performs code review for merge requests and leave comments.

The template contains one auto devops job `chat-review`.

#### How to use
Modify your .gitlab-ci.yml:

1. Add `review` stage to `stages` definition.
2. Include `chat-review.yml` template (`include` keyword of CI configuration).

##### Sample CI config
```yaml
stages:
  - review

include:
  - project: 'CVisionLab/cvl-ci-helpers'
    ref: master # Use tag name to pin specific version
    file: '/ci-templates/chat-review.yml'
```

#### Conventions
1. The job runs only for merge requests.
2. Review results are added as comments to the MR.

#### Configuration
CI includes in GitLab work via deep merging of job definitions. This 
essentially means that configuration of an included job is performed by 
declaring local job with the same name and (re)defining options.

See the next list for the settings that influence jobs defined in this 
template.

* `CHAT_REVIEW_MODEL` defines ChatGPT model used for review.  
  By default `gpt-4-turbo` is used.
* `CHAT_REVIEW_TARGET` defines regex pattern for matching files.  
  By default target regex is `\.(py|c|h|cpp|hpp|cs|sh|(j|mj|t)sx?)$` which enables review for files with the following extensions:
    - Bash/Shell: `sh`
    - C#: `cs`
    - C/C++: `h`, `hpp`, `c`, `cpp`
    - JavaScript: `js`, `jsx`, `mjs`, `mjsx`, `ts`, `tsx`
    - Python: `py`
* `CHAT_REVIEW_GITLAB_TOKEN` is a gitlab token used to interact with gitlab via API.
* `CHAT_REVIEW_LANGUAGE` defines review language.  
  By default review commentaries will be produced in `English`.
* `CHAT_REVIEW_OPENAI_API_KEY` is an OpenAI API key used to interact with ChatGPT.  
  If empty or not exists then value of `OPENAI_API_KEY` variable will be used.  
* `CHAT_REVIEW_OPENAI_HOST` is an OpenAI API host/domain used to interact with ChatGPT.
  This variable is useful when a direct access to `https://api.openai.com` is not possible for some reasons (e.g. firewall).  
  If empty or not exists then value of `OPENAI_HOST` variable will be used.
  If `OPENAI_HOST` is also empty or not exists then `https://api.openai.com` will be used.

Example of configuration overwriting:
```yaml
chat-review:
  variables:
    CHAT_REVIEW_MODEL: "gpt-3.5-turbo"
```


## Sample CI projects

Directory `ci-examples` contains sample projects with configured CI.

*Some files are renamed to avoid special handling of them.*

1. **Python**

   `ci-examples/python/python.gitlab-ci.yml`.

   Simple python package (package contents is omitted) with CI script that 
   runs tests, PEP checks, coverage and setup coverage results display in 
   GitLab.

2. **Qt-based project build (windows cmd)**

   `ci-examples/qt-project-win/qt-winshell.gitlab-ci.yml`.

   Simple build script that only builds tagged releases and release candidates 
   from release branches. It also configures artifacts, so it is possible to 
   download binaries of successful builds.

   *Only CI file is given. Mainly demonstrates artifacts use.*

3. **Qt-based project build with release notes (powershell)**

   `ci-examples/qt-project-win/qt-powershell.gitlab-ci.yml`.

   More featured version of the above build configuration that uses powershell.

   **Features**:
   * Submodules initialization
   * Release notes for tagged builds (contain latest section from Changelog).
   * Release builds:
      - Tagged releases.
      - Tag should match regexp.
      - Create release notes (requires `PRIVATE_CI_TOKEN` variable).
      - Keep build artifacts forever.
   * Release candidate builds:
      -  Release and hotfix branches only.
      -  Do not build for tagged releases.
      -  Artifacts expire in 1 day (and will be deleted).
   * Develop builds:
      - Any changes in `.cpp`, `.c`, `.hpp` or `.h` files.
      - Except tagged builds, master branch, release and hotfix branches.
      - No artifacts.

4. **Qt-based project build separate build stages and inter-project dependencies**

   `ci-examples/qt-project-win/qt-powershell-intra-inter-deps.gitlab-ci.yml`.

   Complex project with intra- and inter- project dependencies.
   Assumes that project:
      * Has multiple products (libraries and demo application).
      * Depends on external library (built in a different Gitlab project).
      * Supports build product configuration via `BUILD` variable (defines 
        what products should be build).

   Most jobs require `PRIVATE_CI_TOKEN` variable.

   **Features**:
   * Submodules initialization
   * Release notes for tagged builds (contain latest section from Changelog).
   * Release builds:
      - Tagged releases.
      - Tag should match regexp.
      - Create release notes.
      - Use released (tagged) versions of the libraries to build demo app.
      - Keep build artifacts forever.
   * Release candidate builds:
      -  Release and hotfix branches only.
      -  Do not build for tagged releases.
      -  Test in-source builds.
      -  Test with pre-built libraries.
      -  Test with released libraries (may fail if release includes 
         incompatible changes in libraries).
      -  Artifacts expire in 1 day (and will be deleted).
   * Develop builds:
      - Any changes in `.cpp`, `.c`, `.hpp` or `.h` files.
      - Except tagged builds, master branch, release and hotfix branches.
      - No artifacts.
   * Dependencies:
      - Not in-source builds fetch build artifacts from the same project, 
        or pass them between stages.
      - Fetch external library build artifacts (tagged release).

5. **Qt-based project build separate build stages and inter-project dependencies (advanced YAML)**

   `ci-examples/qt-project-win/qt-powershell-intra-inter-deps-adv-yml.gitlab-ci.yml`.

   Very similar to the example above but uses advanced YAML features to make 
   job configuration more maintainable.

6. **Build and publish docker images**

   `ci-examples/docker/build-docker-image.gitlab-ci.yml`

   Sample CI that builds docker images for tags matching specific pattern.  
   *This repository uses this CI jobs to build docker images.*

   **Features**:
   * Tag name defines directory to run build (the same CI job can build many images).
   * Supports explicit `latest` tag assignment. A commit can have both version 
     tag and `latest` tag. The latter triggers another job that marks image 
     with corresponding tag.

7. **Qt-based project with both windows and linux build jobs (including .deb)**

   `ci-examples/qt-project-win-and-deb/qt-win-and-deb.gitlab-ci.yml`

   Sample CI that lint python code, build windows binaries and creates debian 
   packages.

   **Features**:
   * Python linting (`flake8`).
   * Check tag version against latest release in changelog for release builds.
   * Building debian package.
      - Separate library and `-dev` packages (the latter includes headers and 
        other files required for development).
      - Automatic generation of debian changelogs (special formatting) using
        project changelog (markdown format).
   * Separate publish step (after building both windows binaries and linux
     packages).
   * Other features like in the above samples.

## Scripts

Scripts directory contains helpers scripts for different environments.
Scripts should only depend on basic utilities available in busybox-like 
environments.

Some scripts may rely on `$PRIVATE_CI_TOKEN` environment variable to make API 
calls.

You can copy helper scripts to your project or use credentials below to add 
repository as a submodule (credentials provide read-only access).

* User name: `gitlab+deploy-token-24422`
* Password: `y9Pnzs6p3dFQ3eyXCoLC`

Subdirectories correspond to execution environments:

* `nixshell` — Unix shell (can be sh, bash, busybox, etc).
* `powershell` — Modern Windows shell.

Available scripts:

1. `nixshell/gitlab.sh`

   Library with functions to interact with Gitlab.

   + `get-pipelines`: Get the last successful pipelines for specific git ref, 
     or matching specific git ref.
   + `get-artifacts`: Download job artifacts archive and extract it to a 
     directory.
   + `get-jobs`: Get the last successful jobs, optionally matching job name 
     against regexp and filtering jobs by artifacts existence.
   + `get-gefs`: Get tags and / or branches of the current or passed commit.

   **Usage samples**

   *Download release from a different job of this project*

   ```bash
   get-pipelines -s tags -r "my-library/v[0-9.]+" -l 1 | \
   get-jobs -a -m "my-lnx64-release" | \
   get-artifacts -d ./deps -r -p | \
   xargs -r unzip -d ./deps
   ```

   *Download release from a different job of a different project*

   ```bash
   get-pipelines -s tags -r "libother/v[0-9.]+" -p "namespace/other" -l 1 | \
   get-jobs -a -m "libother-lnx64-release" | \
   get-artifacts -d ./deps -r -p | \
   xargs -r unzip -d ./deps
   ```

2. `nixshell/debchangelog.awk`

   AWK script to convert changelog in markdown format to changelog in debian format.

   Relies on variables `package` and `author` that should be set on invocation (`-v` command line switch).

3. `poweshell/gitlab.psm1`

   Powershell module with functions to interact with Gitlab. 

   + `Get-Pipelines`: Get the last successful pipelines for specific git ref, 
     or matching specific git ref.
   + `Get-Artifacts`: Download job artifacts archive and extract it to a 
     directory.
   + `Get-Jobs`: Get the last successful jobs, optionally matching job name 
     against regexp and filtering jobs by artifacts existence.
   + `Get-Refs`: Get tags and / or branches of the current or passed commit.
   + `Get-FileSection`: Extracts part of file between two lines matching 
     regular expression. Intended to be used for extracting the last changes 
     from a Changelog to publish them as release notes.
   + `Add-ReleaseNotes`: Add release notes to a tag. Does nothing if release 
     already exists. Tag must exist.
   + `New-Release`: Create new release (Gitlab 11.7+). Release has a name, a
     markdown-enabled description and a set of assets links.
   
   Consult function documentation for additional details (in source or via 
   `Get-Help` after module import).

   **Usage samples**

   *Download release from a different job of this project*

   ```powershell
   Get-Pipelines -Ref-Match "my-library/v[0-9.]+" | \
   Get-Jobs -Job-Match "my-win64-release" -Artifacts | \
   Select-Object -First 1 | \
   Get-Artifacts -Target-Directory "./deps" -If-Not-Exists -Simple-Progress | \
   Expand-Archive -OutputPath .
   ```

   *Download release from a different job of a different project*

   ```powershell
   Get-Pipelines -Scope tags -Ref-Match "libother/v[0-9.]+" -Project "namespace/other" -Limit 1 | \
   Get-Jobs -Job-Match "libother-win64-release" -Artifacts |
   Select-Object -First 1 | \
   Get-Artifacts -Target-Directory "./deps" -If-Not-Exists -Simple-Progress | \
   Expand-Archive -OutputPath .
   ```

4. `nixshell/telegram.sh`

   The script defines single shell function `telegram_notify`. The function can be used to send telegram notifications with custom buttons and supports conditional inclusion of text and buttons.  
   See built-in help (`telegram_notify -h`) or [source code](scripts/nixshell/telegram.sh) for the usage details. 

5. `nixshell/get_changelog_section.awk`

   AWK script to extract a section from the changelog file formatted according 
   to the keepachangelog.com specification.

   The `version` variable is used to define the section to extract. The version 
   should literally match version in the changelog.
   The variable **must** be set on invocation (`-v` command line switch).




## Issue and MR templates (CVisionLab internal)

Issues and Merge Requests (MR) templates are now maintained in the [cvl-golden-repo](https://gitlab.com/CVisionLab/cvl-golden-repo) project.


## Runners (CVisionLab internal)

_GitLab now provides shared Windows runners. Try to use them first. This section (and runners) kept for compatibility._

This repository has several runners that are made available for use in other 
CVisionLab projects.

Shared runners should be explicitly enabled in your project on the CI settings 
page.

1. `win-cmd-x64-vc14-opencv-qt5`

   Shared runner with Windows Server 2016, 64-bit toolchain, Visual Studio 
   build tools 2015 (vc14) and 2017 (vc15), OpenCV 3.4, Qt 5.9, Cuda 9.0,
   Windows cmd shell environment.

   Has [`clcache`](https://github.com/frerich/clcache) installed.

   Installed Cuda components:
   - `compiler_9.0`
   - `command_line_tools_9.0`
   - `cublas_9.0`
   - `cublas_dev_9.0`
   - `cudart_9.0`
   - `cufft_9.0`
   - `cufft_dev_9.0`
   - `curand_9.0`
   - `curand_dev_9.0`
   - `cusolver_9.0`
   - `cusolver_dev_9.0`
   - `cusparse_9.0`
   - `cusparse_dev_9.0`
   - `nvgraph_9.0`
   - `nvgraph_dev_9.0`
   - `npp_9.0`
   - `npp_dev_9.0`
   - `nvrtc_9.0`
   - `nvrtc_dev_9.0`
   - `nvml_dev_9.0`

   Installed cuda patches:
   - Patch 1 (Released Jan 25, 2018)
   - Patch 2 (Released Mar 5, 2018)
   - Patch 3 (Released Jun 7, 2018)
   - Patch 4 (Released Aug 6, 2018)

   Tags:  
   `amd64`, `msvc`, `vc14`, `msvc2015`, `opencv`, `opencv3`, `opencv3.4`, 
   `qt`, `qt5`, `qt5.9`, `win`, `win64`, `x64`, `x86_64`, `cuda`, `cuda9`, 
   `cuda9.0`, `cmd`, `clcache`, `win-cmd-x64-vc14-opencv-qt5`

2. `win-powershell-x64-vc14-opencv-qt5`

   Shared runner with Windows Server 2016, 64-bit toolchain, Visual Studio 
   build tools 2015 (vc14) and 2017 (vc15), OpenCV 3.4, Qt 5.9, Cuda 9.0,
   Windows powershell environment.

   Has [`clcache`](https://github.com/frerich/clcache) installed.

   Installed Cuda components:
   - `compiler_9.0`
   - `command_line_tools_9.0`
   - `cublas_9.0`
   - `cublas_dev_9.0`
   - `cudart_9.0`
   - `cufft_9.0`
   - `cufft_dev_9.0`
   - `curand_9.0`
   - `curand_dev_9.0`
   - `cusolver_9.0`
   - `cusolver_dev_9.0`
   - `cusparse_9.0`
   - `cusparse_dev_9.0`
   - `nvgraph_9.0`
   - `nvgraph_dev_9.0`
   - `npp_9.0`
   - `npp_dev_9.0`
   - `nvrtc_9.0`
   - `nvrtc_dev_9.0`
   - `nvml_dev_9.0`

   Installed cuda patches:
   - Patch 1 (Released Jan 25, 2018)
   - Patch 2 (Released Mar 5, 2018)
   - Patch 3 (Released Jun 7, 2018)
   - Patch 4 (Released Aug 6, 2018)

   Tags:  
   `amd64`, `msvc`, `vc14`, `msvc2015`, `opencv`, `opencv3`, `opencv3.4`, 
   `qt`, `qt5`, `qt5.9`, `win`, `win64`, `x64`, `x86_64`, `cuda`, `cuda9`, 
   `cuda9.0`, `powershell`, `clcache`, `win-powershell-x64-vc14-opencv-qt5`


### Compile cache (clcache)

Runners tagged as `clcache` have installed [clcache](https://github.com/frerich/clcache). Path to the binary added to PATH environment variable.

Default configuration is to use per-project compiler cache for all jobs and runners. Path to the cache directory also includes compiler name to make sure there are no conflicts related to object files generated with different compilers.

Job can can control this behavior using `CLCACHE_DIR` environment variable in conjunction with Gitlab CI caching abilities.
