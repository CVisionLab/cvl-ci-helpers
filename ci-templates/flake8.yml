.flake8:
  stage: lint
  image: "registry.gitlab.com/cvisionlab/cvl-ci-helpers/py3.8-flake8-tox:1.2.0"
  variables:
    # Supported values are: auto, always, changes
    FLAKE8_RUN: auto
  before_script:
    - flake8 --version  # Print out flake8 and plugins versions
  script:
    - flake8 --format gl-codeclimate --output-file flake8-report.json --config "${FLAKE8_CONFIG_PATH}" ${FLAKE8_CHECK_PATHS}
  after_script:
    # Print detected issues to the console
    - |
      <flake8-report.json jq -r 'map("\(.location.path): \(.location.lines.begin)-\(.location.lines.end)\n\t\(.description)\n") | join("\n")'
  artifacts:
    reports:
      codequality: flake8-report.json

flake8:
  extends: .flake8
  variables:
    FLAKE8_CONFIG_PATH: .flake8
  rules:
      # Some rules below can be merged, but at the cost of the reduced clarity.

      # ★ Any mode. Skip branch pipeline if MR pipeline exists.
    - if: '$CI_COMMIT_BRANCH && $CI_PIPELINE_SOURCE == "push" && $CI_OPEN_MERGE_REQUESTS'
      when: never

      # ★ Mode: "auto"
      # Run MR pipeline on changed files, if MR exists.
    - if: '$FLAKE8_RUN == "auto" && $CI_PIPELINE_SOURCE == "merge_request_event"'
      exists:
        - .flake8
      changes:
        - '**/*.py'
      # Run branch pipeline otherwise (regardless of changes).
    - if: '$FLAKE8_RUN == "auto" && $CI_COMMIT_BRANCH'
      exists:
        - .flake8

      # ★ Mode: "changes"
      # Run if there are changes (the above rule performs jobs de-duplication).
    - if: '$FLAKE8_RUN == "changes"'
      exists:
        - .flake8
      changes:
        - '**/*.py'

      # ★ Mode: "always"
      # Run regardless of changes (the above rule performs de-duplication).
    - if: '$FLAKE8_RUN == "always"'
      exists:
        - .flake8


# The following job can be simplified when #283881 will be resolved in GitLab
# https://gitlab.com/gitlab-org/gitlab/-/issues/283881
# Currently variable expansion in rules:exists is not supported.
# The job below must have the same rules as the above job except for the file
# name (.flake8.warn vs .flake8)
flake8-warn:
  extends: .flake8
  variables:
    FLAKE8_CONFIG_PATH: .flake8.warn
  rules:
      # ★ Mode: "auto"
      # Run MR pipeline on changed files, if MR exists.
    - if: '$FLAKE8_RUN == "auto" && $CI_PIPELINE_SOURCE == "merge_request_event"'
      exists:
        - .flake8.warn
      changes:
        - '**/*.py'
      # Run branch pipeline otherwise (regardless of changes).
    - if: '$FLAKE8_RUN == "auto" && $CI_COMMIT_BRANCH'
      exists:
        - .flake8.warn

      # ★ Mode: "changes"
      # Skip branch pipeline if MR exists.
    - if: '$FLAKE8_RUN == "changes" && $CI_COMMIT_BRANCH && $CI_PIPELINE_SOURCE == "push" && $CI_OPEN_MERGE_REQUESTS'
      when: never
      # Run if there are changes (the above rule performs jobs de-duplication).
    - if: '$FLAKE8_RUN == "changes"'
      exists:
        - .flake8.warn
      changes:
        - '**/*.py'

      # ★ Mode: "always"
      # Skip branch pipeline if MR exists.
    - if: '$FLAKE8_RUN == "always" && $CI_COMMIT_BRANCH && $CI_PIPELINE_SOURCE == "push" && $CI_OPEN_MERGE_REQUESTS'
      when: never
      # Run regardless of changes (the above rule performs de-duplication).
    - if: '$FLAKE8_RUN == "always"'
      exists:
        - .flake8.warn
  allow_failure: true

