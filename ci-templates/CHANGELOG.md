# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## Unreleased
Add unreleased changes below. Do not remove this line.

## [v1.11.0] - 2024-12-11
### Changed
* Updated Ruff template to modern ruff CLI.
* Updated Ruff template to use a fixed version of docker image by default.

[v1.11.0]: https://gitlab.com/CVisionLab/cvl-ci-helpers/-/compare/ci/templates/v1.10.0...ci/templates/v1.11.0


## [v1.10.0] - 2024-04-17
### Added
* Added [chat-review](https://github.com/ikoofe/chat-review)-based template for automatic code review.  
  The review tool is based on ChatGPT and requires access to the OpenAI API, which must be configured via the environment.

[v1.10.0]: https://gitlab.com/CVisionLab/cvl-ci-helpers/-/compare/ci/templates/v1.9.0...ci/templates/v1.10.0


## [v1.9.0] - 2023-09-29
### Added
* Added [ruff](https://docs.astral.sh/ruff/)-based template for linting.  
  The linter is highly configurable and blazing fast.

[v1.9.0]: https://gitlab.com/CVisionLab/cvl-ci-helpers/-/compare/ci/templates/v1.8.0...ci/templates/v1.9.0


## [v1.8.0] - 2023-04-24
### Added
* Docker build jobs now support symbolic links in the context directories.  
  The jobs now use `tar` to resolve symbolic links and send context to the docker.

[v1.8.0]: https://gitlab.com/CVisionLab/cvl-ci-helpers/-/compare/ci/templates/v1.7.0...ci/templates/v1.8.0


## [v1.7.0] - 2023-03-07
### Added
* Added [flakeheaven](https://flakeheaven.readthedocs.io/index.html)-based template for linting.  
  This linter allows fine-grained configuration of rules and enabled plugins.

[v1.7.0]: https://gitlab.com/CVisionLab/cvl-ci-helpers/-/compare/ci/templates/v1.6.1...ci/templates/v1.7.0


## [v1.6.2] - 2022-09-19
### Fixed
* Templates now use `CI_API_V4_URL` variable to access API.

[v1.6.2]: https://gitlab.com/CVisionLab/cvl-ci-helpers/-/compare/ci/templates/v1.6.1...ci/templates/v1.6.2


## [v1.6.1] - 2022-09-09
### Changed
* The docker image registry changed to specific `registry.gitlab.com` instead of `$CI_REGISTRY` variable.
  This simplifies use of templates from other gitlab instances.

[v1.6.1]: https://gitlab.com/CVisionLab/cvl-ci-helpers/-/compare/ci/templates/v1.6.0...ci/templates/v1.6.1


## [v1.6.0] - 2021-11-16
### Added
* The `calc-issue-time` job template (required stage `issue-time`).  
  The job makes reports with info about issue spent time. Required CI environment variables: `GITLAB_TOKEN` and `NUMBER_OF_LAST_DAYS`. `GITLAB_TOKEN` is a token value for GitLab API access. `NUMBER_OF_LAST_DAYS` is a number of days taken into account when generating reports. The job takes the required number of days before today, including today.
* Added auto devops template for release publishing.
* Added `TELEGRAM_DRY_RUN` variable in Telegram notification templates. If set (any non empty value), disables mesage delivery and instead prints to the console.
* Added `TELEGRAM_EXTRA_BUTTONS_LAYOUT` in Telegram notification templates. The variable controls layout of the additional buttons. Possible values are `rows`, `cols` or `none`. Default is `rows`.
* Added support for `TELEGRAM_BUTTONS_BUTTON_*` variables (`*` means anything) in Telegram notification templates.  
  Each variable is expected to contain a single button definition in the `text=url` format.
* All Telegram notfication templates now include definitions of two shell functions: `telegram_notify` and `telegram_notify_ci_status`. The first function is a generic function that can send Telegram notifications with custom buttons. The second function is a helper function that simply calls `telegram_notify` with pre-populated messages about the current CI pipeline status.

### Changed
* Renamed `SEND_TIMEOUT` environment variable to `TELEGRAM_SEND_TIMEOUT` in Telegram notification templates.

### Deprecated
* The `TELEGRAM_BUTTONS_EXTRA` variable in Telegram notification templates is now deprecated.  
  While removal date is not defined, all new code should use `TELEGRAM_BUTTONS_BUTTON_*` variables instead.

[v1.6.0]: https://gitlab.com/CVisionLab/cvl-ci-helpers/-/compare/ci/templates/v1.5.0...ci/templates/v1.6.0


## [v1.5.0] - 2021-04-09
The project is now licensed under the MIT license.

### Changed
* Removed explicit declaration of telegram bot token. The token must be explicitly provided via `TELEGRAM_BOT_TOKEN` variable.  
  **The previous token made invalid, all projects must update.**

[v1.5.0]: https://gitlab.com/CVisionLab/cvl-ci-helpers/-/compare/ci/templates/v1.4.0...ci/templates/v1.5.0


## [1.4.0] - 2021-03-01
### Added
* Flake8 jobs now publish their results as CodeClimate reports visible in GitLab UI.
* Added support for using `flake8` jobs with branch pipelines.
* Added `FLAKE8_RUN` variable to control flake8 jobs behavior.  
  Supported values are: `auto` (default), `changes` and `always`.
  - In `auto` mode, jobs run if any `.py` file changes in MR pipelines and always in branch pipelines.
  - In `changes` mode, jobs only run if there are changed `.py` files (this can lead to surprising behavior if using branch pipelines).
  - If set to `always` mode, jobs are executed regardless of changed `.py` files.
  - All modes are configured to perform jobs de-duplication: if both branch and MR pipelines are allowed by the project configuration, branch pipeline is skipped if MR exists.
* Telegram notifications now recognize additional entities:
  - Tags pipelines. If running for a tag, replaces "Branch: ..." text.
  - Merge requests. If running for a merge request, adds an additional text line and a button.
  - Deployments (GitLab environments). If configured, adds an additional text line and a button.  
    Requires special setup to pass information about environment to the notification job.
* Telegram notifications now supports configuration via additional variables:
  - `TELEGRAM_BUTTONS_CONFIG` defines notification buttons configuration.  
    Default is `Pipeline,Report,Commit,MR;Deployment`.  
    Commas define buttons order within a single row. Semicolons define button
    rows.
  - `TELEGRAM_BUTTONS_EXTRA` defines additional buttons.  
    Default is `` (empty).  
    Accepts semicolon-separated list of `name=value` pairs. The `name` defines 
    button text and the value defines button URL.  
    For example: `button-1=https://my-button/one;button=2=https://my-button/two`
* Job template for building and pushing package to project package registry.

### Changed
* Flake8 jobs switched to a newer docker image with more `flake8` plugins installed and **enabled by default**.

[1.4.0]: https://gitlab.com/CVisionLab/cvl-ci-helpers/-/compare/ci/templates/v1.3.1...ci/templates/v1.4.0


## [1.3.1] - 2021-01-22
### Fixed
* Fixed push of docker images without tag. Looks like the docker has changed behavior and push command without tag name refers to the latest tag only.

[1.3.1]: https://gitlab.com/CVisionLab/cvl-ci-helpers/-/compare/ci/templates/v1.3.0...ci/templates/v1.3.1


## [1.3.0] - 2020-10-31
### Added
* Telegram: Notification now can be skipped.  
  The notifications are skipped if the commit message includes one of:  
  `[mute]`, `[no_bell]`, `[silent]`, `:mute:`, `:no_bell:` or `:silent:`.  
  Note: `:mute:` and `:no_bell:` are actually rendered by GitLab Markdown as :mute: and :no_bell: accordingly.


### Fixed
* Telegram: Notification jobs now skip repository checkout and artifacts downloading.

[1.3.0]: https://gitlab.com/CVisionLab/cvl-ci-helpers/-/compare/ci/templates/v1.2.0...ci/templates/v1.3.0


## [1.2.0] - 2020-09-08
### Added
* Added new CI template for Telegram notifications.  
  The default target is CVL GitLab Bot, but can be customized via environment 
  variable.

[1.2.0]: https://gitlab.com/CVisionLab/cvl-ci-helpers/-/compare/ci/templates/v1.1.2...ci/templates/v1.2.0

## [1.1.2] - 2019-11-21
### Fixed
* Fixed `flake8` template jobs `.yml` comparison against undefined variable.  
  If a variable is not set then comparison against empty string fails in GitLab
  (but works in shell).

### Added
* `mypy` job updated to skip tagged jobs.

[1.1.2]: https://gitlab.com/CVisionLab/cvl-ci-helpers/compare/gitlab-helpers/ci/templates/v1.1.1...gitlab-helpers/ci/templates/v1.1.2


## [1.1.1] - 2019-11-20
### Fixed
* Fixed `flake8` template jobs `.yml` configuration error.  
  GitLab doesn't allow to have both `except` / `only` and `rules` in one job.  
  Switched to `rules` completely.

[1.1.1]: https://gitlab.com/CVisionLab/cvl-ci-helpers/compare/gitlab-helpers/ci/templates/v1.1.0...gitlab-helpers/ci/templates/v1.1.1


## [1.1.0] - 2019-11-18
### Added
* Added a new job `docker-tag` to `multi-docker` template.  
  The job tags existing docker images with other names to create aliases.
  Tagging syntax: `docker/<dst-img>/<dst-tag>=<src-img>/<src-tag>`.

[1.1.0]: https://gitlab.com/CVisionLab/cvl-ci-helpers/compare/gitlab-helpers/ci/templates/v1.0.0...gitlab-helpers/ci/templates/v1.1.0


## 1.0.0 - 2019-11-16

The first release of CI templates.

The following auto jobs templates are implemented:
- `docker` - Simple docker workflow with one docker image per project.  
  Automatically build docker image for `docker/<version>`- tagged commits from 
  `docker/Dockerfile` definitions.
- `multi-dcoker` - More advanced version of the above workflow supporting 
  multiple docker images per project.  
  Automatically build docker image for `docker/<image>/<version>`- tagged 
  commits from `docker/<image>/Dockerfile` definitions.
- `flake8` - Run `flake8`-based linting on changes in python files.  
  Defines two jobs: check for errors and check for warnings. Jobs are executed 
  only if corresponding configuration file exists in project root (`.flake8` 
  or `.flake8.warn`).  
  Uses size-optimized docker image.
- `mypy` - Run `mypy`-based static type checking on changes in python files.  
  Job is only executed if corresponding configuration file exists in project 
  root (`.mypy`).  
  Uses size-optimized docker image.
